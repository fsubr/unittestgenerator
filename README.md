# Unit Test Skeleton Generator

Started cca 2012. Tested on multiple project of various sizes.

Read php files from given directory, find classes and its methods. For each immediate public method generate automatic test skeleton. 

## What is characterisation test

- helps start testing legacy code
- barren coverage
- mutation testing can help you improve asserts in generated tests
- benefits of running code, linting and static analysis is not enough
- see [case study](docs/case-study.md) of Utg used on Utg

## How to use UTG

- run `composer install`
- create `preset`
- run `./utg.php`

## How to create `preset`

- configuration
- templates fragments

## How to run tests

Run `composer tests:all`

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/bac8d8acf47747e09a106ec71cb389c4)](https://app.codacy.com/bb/fsubr/unittestgenerator/dashboard?utm_source=bb&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/bac8d8acf47747e09a106ec71cb389c4)](https://app.codacy.com/bb/fsubr/unittestgenerator/dashboard?utm_source=bb&utm_medium=referral&utm_content=&utm_campaign=Badge_coverage)
