<?php

declare(strict_types=1);

namespace UnitTestGenerator\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnitTestGenerator\Finder\Classes\FindClasses;
use UnitTestGenerator\Generator\Preset\PresetFactory;
use UnitTestGenerator\Generator\Preset\PresetNotFound;
use UnitTestGenerator\Reflector\FileReflection;

class ListClasses extends Command {

    private PresetFactory $presetFactory;
    private FindClasses $finder;

    public function __construct(PresetFactory $presetFactory, FindClasses $finder, string $name = null) {
        parent::__construct($name);
        $this->presetFactory = $presetFactory;
        $this->finder = $finder;
    }

    protected function configure(): void {
        $this->setName('classes');
        $this->setDescription('List classes');
        $this->addArgument(
            'preset',
            InputArgument::OPTIONAL,
            'Preset name',
            'default'
        );
    }

    /**
     * @throws PresetNotFound
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        $preset = $input->getArgument('preset');
        if (!is_string($preset)) {
            throw new PresetNotFound(sprintf('Preset name should be a string. %s given', gettype($preset)));
        }
        $config = $this->presetFactory->load($preset);

        $begin = microtime(true);

        $files = $this->finder->get($config->getInputPath(), $config->getExtensions(), $config->getExclude());

        $classesFound = count($files);

        $progressBar = new ProgressBar($output, $classesFound);
        $progressBar->start();

        $result = [];

        foreach ($files as $file) {
            $reflector = new FileReflection($file);
            $classes = $reflector->getAllClasses();
            foreach ($classes as $class) {
                $name = $class->getName();
                $result[$name][] = $file;
            }
            $progressBar->advance();
        }

        $progressBar->finish();

        $duplicatesFound = 0;
        $output->writeln('');
        foreach ($result as $fqn => $fqnFiles) {
            if (count($fqnFiles) > 1) {
                $duplicatesFound++;
                $output->writeln($fqn);
                foreach ($fqnFiles as $file) {
                    $output->writeln($file);
                }
                $output->writeln('');
            }
        }

        $output->writeln('');
        $output->writeln(sprintf('<info>Duplicates found: %s</info>', $duplicatesFound));
        $today = date('j.n.Y H:i:s');
        $output->writeln(sprintf('<info>%s</info>', $today));
        $duration = microtime(true) - $begin;
        $output->writeln(sprintf('<info>Duration: %s</info>', $duration));

        return Command::SUCCESS;
    }
}
