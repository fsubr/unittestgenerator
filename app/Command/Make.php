<?php

declare(strict_types=1);

namespace UnitTestGenerator\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnitTestGenerator\Finder\Classes\FindClasses;
use UnitTestGenerator\Generator\Preset\PresetFactory;
use UnitTestGenerator\Generator\Preset\PresetNotFound;
use UnitTestGenerator\Generator\UtgFactory;

class Make extends Command {

    private PresetFactory $presetFactory;
    private UtgFactory $generatorFactory;
    private FindClasses $finder;

    public function __construct(PresetFactory $presetFactory, UtgFactory $utgFactory, FindClasses $finder, string $name = null) {
        parent::__construct($name);
        $this->presetFactory = $presetFactory;
        $this->generatorFactory = $utgFactory;
        $this->finder = $finder;
    }

    protected function configure(): void {
        $this->setName('make');
        $this->setDescription('Generate tests skeletons');
        $this->addArgument(
            'preset',
            InputArgument::OPTIONAL,
            'Preset name',
            'default'
        );
    }

    /**
     * @throws PresetNotFound
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        $preset = $input->getArgument('preset');
        if (!is_string($preset)) {
            throw new PresetNotFound(sprintf('Preset name should be a string. %s given', gettype($preset)));
        }
        $config = $this->presetFactory->load($preset);

        $output->writeln(sprintf('<info>Read project dir %s</info>', $config->getInputPath()));

        $classes = $this->finder->get($config->getInputPath(), $config->getExtensions(), $config->getExclude());
        $classesFound = count($classes);
        $output->writeln(sprintf('<comment>Found %s classes</comment>', $classesFound));

        $output->writeln(sprintf('<info>Generate new tests to %s</info>', $config->getOutputPath()));

        $begin = microtime(true);

        $generator = $this->generatorFactory->createRoave(
            $config->getInputPath(),
            $config->getOutputPath(),
            $config->getTemplatePath(),
            $config->getValuesProvider()
        );

        $progressBar = new ProgressBar($output, $classesFound);
        $progressBar->start();

        $testsGenerated = 0;
        foreach ($classes as $class) {
            $generator->generate($class);
            $testsGenerated++;

            $progressBar->advance();
        }
        $progressBar->finish();

        $output->writeln('');
        $output->writeln('');
        $output->writeln(sprintf('<info>Generated tests: %s</info>', $testsGenerated));
        $today = date('j.n.Y H:i:s');
        $output->writeln(sprintf('<info>%s</info>', $today));
        $duration = microtime(true) - $begin;
        $output->writeln(sprintf('<info>Duration: %s</info>', $duration));

        return Command::SUCCESS;
    }
}
