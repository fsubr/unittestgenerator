<?php

declare(strict_types=1);

namespace UnitTestGenerator\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Preset extends Command {


    protected function configure(): void {
        $this->setName('preset');
        $this->setDescription('Manage UTG presets');
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        $files = (array)glob('presets/*.php');

        $output->writeln('<info>List of defined presets</info>');
        $replace = [];
        $replace['presets/'] = '';
        $replace['.php'] = '';
        foreach ($files as $file) {
            $name = str_replace(array_keys($replace), $replace, (string)$file);
            $output->writeln(sprintf('<comment>%s</comment>', $name));
        }

        return Command::SUCCESS;
    }
}
