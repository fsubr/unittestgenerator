<?php

declare(strict_types=1);

namespace UnitTestGenerator\Config;

use Corason\Container\Container;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\ConsoleOutput;
use UnitTestGenerator\Command\ListClasses;
use UnitTestGenerator\Command\Make;
use UnitTestGenerator\Command\Preset;
use UnitTestGenerator\Filesystem\GenericFilesystem;
use UnitTestGenerator\Finder\Classes\FindClasses;
use UnitTestGenerator\Generator\Preset\PresetFactory;
use UnitTestGenerator\Generator\UtgFactory;
use UnitTestGenerator\Generator\Values\BasicExpectationValueGenerator;
use UnitTestGenerator\Generator\Values\BasicParameterValueGenerator;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\ReflectionFactory;

class UtgContainer extends Container {

    private string $appName = 'UnitTestGenerator';
    private int $dirPermission = 0644;

    protected function getApplication(): Application {
        $application = new Application($this->appName);
        $application->add($this->getMake());
        $application->add($this->getPreset());
        $application->add($this->getListClasses());
        return $application;
    }

    private function getListClasses(): ListClasses {
        return new ListClasses($this->getPresetFactory(), $this->getFindClasses());
    }

    private function getPreset(): Preset {
        return new Preset();
    }

    protected function getConsoleOutput(): ConsoleOutput {
        return new ConsoleOutput();
    }

    private function getMake(): Make {
        return new Make($this->getPresetFactory(), $this->getGeneratorFactory(), $this->getFindClasses());
    }

    private function getPresetFactory(): PresetFactory {
        return new PresetFactory($this->getValuesProvider());
    }

    private function getValuesProvider(): ValuesProvider {
        $generator = new ValuesProvider($this->getLogger());
        $generator->registerExpectationGenerator(new BasicExpectationValueGenerator());
        $generator->registerParameterGenerator(new BasicParameterValueGenerator());
        return $generator;
    }

    private function getGeneratorFactory(): UtgFactory {
        return new UtgFactory($this->getLogger(), $this->getStorage(), $this->getReflectionFactory());
    }

    private function getLogger(): ConsoleLogger {
        return new ConsoleLogger($this->getConsoleOutput());
    }

    private function getStorage(): GenericFilesystem {
        return new GenericFilesystem($this->getLogger(), $this->dirPermission);
    }

    private function getFindClasses(): FindClasses {
        return new FindClasses($this->getReflectionFactory());
    }

    private function getReflectionFactory(): ReflectionFactory {
        return new ReflectionFactory();
    }
}
