<?php

declare(strict_types=1);

namespace UnitTestGenerator\Filesystem;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;

interface Filesystem {


    public function write(string $filename, string $data): void;

    /**
     * @throws FileNotFoundException
     */
    public function read(string $filename): string;
}
