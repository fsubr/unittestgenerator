<?php

declare(strict_types=1);

namespace UnitTestGenerator\Filesystem;

use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class GenericFilesystem implements Filesystem {

    private LoggerInterface $logger;
    private int $dirPermission;

    public function __construct(LoggerInterface $logger, int $dirPermission) {
        $this->logger = $logger;
        $this->dirPermission = $dirPermission;
    }

    /**
     * @throws RuntimeException
     */
    public function write(string $filename, string $data): void {
        $parts = pathinfo($filename);

        if (isset($parts['dirname']) && !file_exists($parts['dirname'])) {
            $dirName = $parts['dirname'];
            $result = @mkdir($dirName, $this->dirPermission, true);
            if ($result === false && !is_dir($dirName)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $dirName));
            }
        }

        $result = @file_put_contents($filename, $data);
        if ($result === false) {
            $this->logger->error(sprintf('Error writing to %s.', $filename));
        }
    }

    /**
     * @throws FileNotFoundException
     */
    public function read(string $filename): string {
        if (file_exists($filename)) {
            $contents = file_get_contents($filename);
            if ($contents !== false) {
                return $contents;
            }
        }
        $this->logger->error(sprintf('File %s not found.', $filename));
        throw new FileNotFoundException(sprintf('File %s not found.', $filename));
    }
}
