<?php

declare(strict_types=1);

namespace UnitTestGenerator\Finder\Classes;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use UnitTestGenerator\Reflector\ReflectionFactory;

/**
 * Finder Class filter.
 */
class FindClasses {

    /**
     * Included extension of processed files.
     *
     * @var string[]
     */
    private array $extensions;
    /**
     * List of excluded files.
     *
     * Accepts even partial paths.
     *
     * @var string[]
     */
    private array $exclude = [];
    private ReflectionFactory $reflectionFactory;

    public function __construct(ReflectionFactory $reflectionFactory) {
        $this->reflectionFactory = $reflectionFactory;
    }

    /**
     * Return array of files from $projectDir which fulfils criteria.
     *
     * @param string[] $extensions
     * @param string[] $exclude
     *
     * @return array<int, non-empty-string>
     */
    public function get(string $projectDir, array $extensions, array $exclude = []): array {
        $this->extensions = $extensions;
        $this->exclude = $exclude;

        $ite = new RecursiveDirectoryIterator($projectDir);

        $files = [];
        foreach (new RecursiveIteratorIterator($ite) as $file) {
            /** @var SplFileInfo $file */
            $pathName = $this->canonicalize($file->getPathname());
            if ($pathName !== '' && $this->includeFile($pathName)) {
                $files[] = $pathName;
            }
        }

        return $files;
    }

    /**
     * Return true if file extension is listed in $extensions array.
     *
     * @param string $file File path
     */
    private function includeExtension(string $file): bool {
        $pathParts = pathinfo($file);
        if (!isset($pathParts['extension'])) {
            $pathParts['extension'] = '';
        }
        if (in_array($pathParts['extension'], $this->extensions, true)) {
            return true;
        }
        return false;
    }

    /**
     * Return false if file path doesn't match $exclude.
     *
     * @param string $file File path
     */
    private function isExcluded(string $file): bool {
        foreach ($this->exclude as $needle) {
            if (stripos($file, $needle) !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return true if file should be included in fileset.
     *
     * @param string $file File path
     */
    private function includeFile(string $file): bool {
        if (!$this->includeExtension($file)) {
            return false;
        }
        if ($this->isExcluded($file)) {
            return false;
        }
        if (!$this->containClasses($file)) {
            return false;
        }
        return true;
    }

    /**
     * Return true if file contains php class.
     *
     * @param string $file File path
     */
    private function containClasses(string $file): bool {
        return $this->reflectionFactory->streamFromFile($file)->hasClass();
    }

    /**
     * Canonicalize path.
     *
     * @param string $path Filesystem path
     */
    private function canonicalize(string $path): string {
        $replace = [];
        $replace['\\'] = '/';
        $replace['//'] = '/';
        return str_replace(array_keys($replace), $replace, $path);
    }
}
