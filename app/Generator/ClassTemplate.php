<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator;

use Psr\Log\LoggerInterface;
use ReflectionMethod;
use Roave\BetterReflection\Reflector\Exception\IdentifierNotFound;
use RuntimeException;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\ClassReflection;
use UnitTestGenerator\Reflector\MethodReflection;

class ClassTemplate {

    protected const EOL = "\n";
    /**
     * Path to directory with custom skeleton templates.
     */
    private string $templateDir;
    /**
     * Templates names for skeletons.
     *
     * @var string[]
     */
    private array $template = [
        'class' => '/ClassSkeleton.php.txt',
        'setup' => '/MethodSetupSkeleton.php.txt',
        'method' => '/MethodSkeleton.php.txt',
        'staticMethod' => '/MethodStaticSkeleton.php.txt',
    ];

    private LoggerInterface $logger;
    private ValuesProvider $valuesProvider;

    public function __construct(string $templateDir, LoggerInterface $logger, ValuesProvider $valuesProvider) {
        $this->valuesProvider = $valuesProvider;
        $this->setTemplateDir($templateDir);
        $this->logger = $logger;
    }

    /**
     * Returns template filled with $data.
     *
     * @param ClassReflection $class Class reflector
     *
     * @throws RuntimeException
     */
    public function render(ClassReflection $class): string {
        $className = $class->getShortName();

        $immediateMethods = $class->getImmediatePublicMethods();

        if (count($immediateMethods) === 0) {
            throw new RuntimeException(sprintf('No public methods in %s class.', $className));
        }

        if (count($immediateMethods) === 1 && $immediateMethods[0]->isConstructor() === true) {
            throw new RuntimeException(sprintf('No test generated for %s class with just constructor.', $className));
        }

        $methods = '';
        $immediateConstructor = false;
        foreach ($immediateMethods as $method) {
            $methodName = $method->getName();
            $parameterList = $this->generateParameterList($method);
            $parameterInitList = $this->generateParameterDefinitions($method);
            $expectation = $this->valuesProvider->getExpectationValue($method);
            $skeleton = $this->getMethodSkeleton($method);
            $methods .= $this->fillMethodSkeleton($className, $methodName, $parameterList, $parameterInitList, $expectation, $skeleton);

            if ($method->isConstructor()) {
                $immediateConstructor = true;
            }
        }

        if ($immediateConstructor === false) {
            $skeleton = (string)file_get_contents($this->templateDir.$this->template['setup']);
            $methods = $this->fillMethodSkeleton($className, '', '', '', '', $skeleton).$methods;
        }

        $namespaceName = $class->getNamespaceName();
        $fullClassName = $class->getName();
        return $this->fillClassSkeleton($namespaceName, $className, $methods, $fullClassName);
    }

    private function setTemplateDir(string $dir): void {
        $this->templateDir = $dir;
    }

    private function fillClassSkeleton(string $namespaceName, string $className, string $methods, string $fullClassName): string {
        $skeleton = $this->getClassSkeleton();

        $replace = [];
        $replace['{namespace}'] = $namespaceName;
        $replace['namespace ;'.self::EOL] = '';
        $replace['\;'.self::EOL] = ';'.self::EOL;
        $replace['{classname}'] = $className;
        $replace['{fullclassname}'] = $fullClassName;
        $replace['{lcclassname}'] = lcfirst($className);
        $replace['    {methods}'.self::EOL] = $methods;

        return str_replace(array_keys($replace), $replace, $skeleton);
    }

    private function fillMethodSkeleton(string $className, string $methodName, string $parameterList, string $parameterInitList, string $expectation, string $skeleton): string {
        $replace = [];
        $replace['{classname}'] = $className;
        $replace['{lcclassname}'] = lcfirst($className);
        $replace['{method}'] = $methodName;
        $replace['{ucmethod}'] = ucfirst($methodName);
        $replace['{parameters}'] = $parameterList;
        $replace['{expectation}'] = $expectation;
        if ($parameterInitList === '') {
            $replace['        {parametersInit}'.self::EOL] = $parameterInitList;
        } else {
            $replace['{parametersInit}'] = $parameterInitList;
        }

        return str_replace(array_keys($replace), $replace, $skeleton);
    }

    private function getClassSkeleton(): string {
        return (string)file_get_contents($this->templateDir.$this->template['class']);
    }

    private function getMethodSkeleton(MethodReflection $method): string {
        if ($method->isConstructor()) {
            return (string)file_get_contents($this->templateDir.$this->template['setup']);
        }
        if (($method->getModifiers() & ReflectionMethod::IS_STATIC) === ReflectionMethod::IS_STATIC) {
            return (string)file_get_contents($this->templateDir.$this->template['staticMethod']);
        }
        return (string)file_get_contents($this->templateDir.$this->template['method']);
    }

    private function generateParameterList(MethodReflection $method): string {
        $out = '';
        foreach ($method->getParameters() as $parameter) {
            $out .= '$'.$parameter->getName().', ';
        }
        return substr($out, 0, -2);
    }

    private function generateParameterDefinitions(MethodReflection $method): string {
        $out = '';

        foreach ($method->getParameters() as $parameter) {
            $comment = '';
            try {
                $value = $this->valuesProvider->getParameterValue($parameter);
            } catch (IdentifierNotFound) {
                // Default value is for example class constant which was not autoloaded yet
                $msg = sprintf('Use fallback parameter value for %s in %s', $parameter->getName(), $parameter->getFileName());
                $this->logger->info($msg);
                $value = 'null';
                $comment = ' // FIXXME Use fallback value';
            }
            $out .= '$'.$parameter->getName().' = '.$value.';'.$comment.self::EOL.'        ';
        }
        return substr($out, 0, mb_strlen(self::EOL.'        ') * -1);
    }
}
