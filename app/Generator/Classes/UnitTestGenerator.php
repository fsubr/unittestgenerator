<?php

/**
 * Unit test generator.
 */

declare(strict_types=1);

namespace UnitTestGenerator\Generator\Classes;

use Exception;
use Psr\Log\LoggerInterface;
use UnitTestGenerator\Filesystem\Filesystem;
use UnitTestGenerator\Generator\ClassTemplate;
use UnitTestGenerator\Generator\PathsHelper;
use UnitTestGenerator\Reflector\ReflectionFactory;

class UnitTestGenerator {

    protected ClassTemplate $classTemplate;
    protected PathsHelper $pathsHelper;
    private LoggerInterface $logger;
    private Filesystem $storage;
    private ReflectionFactory $reflectionFactory;

    public function __construct(ClassTemplate $classTemplate, PathsHelper $pathsHelper, LoggerInterface $logger, Filesystem $storage, ReflectionFactory $reflectionFactory) {
        $this->classTemplate = $classTemplate;
        $this->pathsHelper = $pathsHelper;
        $this->logger = $logger;
        $this->storage = $storage;
        $this->reflectionFactory = $reflectionFactory;
    }

    /**
     * Generates unit test skeletons for classes in source file.
     *
     * @param non-empty-string $file Php source file
     */
    public function generate(string $file): void {
        $fileReflection = $this->reflectionFactory->fromFile($file);
        $classes = $fileReflection->getAllClasses();

        foreach ($classes as $class) {
            try {
                $unitTest = $this->classTemplate->render($class);
                $unitTestClass = $class->getShortName();

                $filename = $this->pathsHelper->getTestPath($file, $unitTestClass);
                $this->storage->write($filename, $unitTest);
            } catch (Exception $exception) {
                $this->logger->error($exception->getMessage());
                $this->logger->error(sprintf('Processing file %s', $class->getFileName()));
            }
        }
    }
}
