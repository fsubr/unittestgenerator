<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator;

class DataProviderMatrix {


    /**
     * @param array<string, array<int, mixed>> $input
     *
     * @return array<non-falsy-string, non-empty-array<string, mixed>>
     */
    public function generate(string $prefix, array $input, mixed $expectedValue): array {
        $combinations = $this->getCombinations($input);
        $result = [];

        foreach ($combinations as $index => $combination) {
            $key = $prefix.'-'.($index + 1);
            $result[$key] = array_merge($combination, ['expected' => $expectedValue]);
        }

        return $result;
    }

    /**
     * @param array<string, array<int, mixed>> $input
     *
     * @return array<int, array<string, mixed>>
     */
    private function getCombinations(array $input): array {
        $keys = array_keys($input);
        $combinations = $this->generateCombinations($input, $keys);

        return array_map(static function ($combination) use ($keys) {
            return array_combine($keys, $combination);
        }, $combinations);
    }

    /**
     * @param array<string, array<int, mixed>> $input
     * @param string[] $keys
     *
     * @return array<int<0, max>, array<string, mixed>>
     */
    private function generateCombinations(array $input, array $keys): array {
        $combinations = [[]];

        foreach ($keys as $key) {
            $newCombinations = [];

            foreach ($combinations as $combination) {
                foreach ($input[$key] as $value) {
                    $newCombinations[] = array_merge($combination, [$key => $value]);
                }
            }

            $combinations = $newCombinations;
        }

        return $combinations;
    }
}
