<?php

/**
 * Helps with file paths resolving
 */

declare(strict_types=1);

namespace UnitTestGenerator\Generator;

class PathsHelper {

    /**
     * Path to project files. Input dir.
     */
    private string $projectDir;
    /**
     * Path to test directory. Output dir.
     */
    private string $testsDir;

    public function __construct(string $projectDir, string $testsDir) {
        $this->projectDir = $projectDir;
        $this->testsDir = $testsDir;
    }

    /**
     * Return path to unit test file.
     *
     * @param string $classFile File with class
     * @param string $className Class name
     */
    public function getTestPath(string $classFile, string $className): string {
        $relativeClassPath = str_replace($this->canonicalize($this->projectDir), '', $this->canonicalize($classFile));
        if ($this->testsDir !== '') {
            $relativeClassPath = '/'.$relativeClassPath;
        }
        $resultTestFile = $this->canonicalize($this->testsDir.$relativeClassPath);

        $pathParts = pathinfo($resultTestFile);
        $path = str_replace($pathParts['basename'], $className.'Test.php', $resultTestFile);
        return $this->canonicalize($path);
    }

    /**
     * Canonicalize path.
     *
     * @param string $path Filesystem path
     */
    private function canonicalize(string $path): string {
        $replace = [];
        $replace['\\'] = '/';
        $replace['//'] = '/';
        return str_replace(array_keys($replace), $replace, $path);
    }
}
