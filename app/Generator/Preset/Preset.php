<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator\Preset;

use UnitTestGenerator\Generator\Values\ValuesProvider;

class Preset {

    private string $inputPath;
    /** @var string[] */
    private array $exclude;
    private string $outputPath;
    private string $templatePath;
    /** @var string[] */
    private array $extensions;
    private ValuesProvider $valuesProvider;

    /**
     * @param string[] $exclude
     * @param string[] $extensions
     */
    public function __construct(string $inputPath, string $outputPath, string $templatePath, array $exclude, array $extensions, ValuesProvider $valuesProvider) {
        $this->inputPath = $inputPath;
        $this->outputPath = $outputPath;
        $this->templatePath = $templatePath;
        $this->exclude = $exclude;
        $this->extensions = $extensions;
        $this->valuesProvider = $valuesProvider;
    }

    public function getInputPath(): string {
        return $this->inputPath;
    }

    /**
     * @return string[]
     */
    public function getExclude(): array {
        return $this->exclude;
    }

    public function getOutputPath(): string {
        return $this->outputPath;
    }

    public function getTemplatePath(): string {
        return $this->templatePath;
    }

    /**
     * @return string[]
     */
    public function getExtensions(): array {
        return $this->extensions;
    }

    public function getValuesProvider(): ValuesProvider {
        return $this->valuesProvider;
    }
}
