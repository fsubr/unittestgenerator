<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator\Preset;

use UnitTestGenerator\Generator\Values\ValuesProvider;

class PresetFactory {

    private ValuesProvider $valuesProvider;

    public function __construct(ValuesProvider $valuesProvider) {
        $this->valuesProvider = $valuesProvider;
    }

    /**
     * @throws PresetNotFound
     */
    public function load(string $presetPath): Preset {
        $presetFile = sprintf('presets/%s.php', $presetPath);

        if (file_exists($presetFile)) {
            // TODO config v YAMLu nebo jako potomek Preset?
            $inputPath = '';
            $templatePath = '';
            $outputPath = '';
            $exclude = [];
            $extensions = ['php'];
            require_once $presetFile;
            return new Preset($inputPath, $outputPath, $templatePath, $exclude, $extensions, $this->valuesProvider);
        }
        throw new PresetNotFound(sprintf('Preset file %s does not exists!', $presetFile));
    }
}
