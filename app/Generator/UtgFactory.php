<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator;

use Psr\Log\LoggerInterface;
use UnitTestGenerator\Filesystem\Filesystem;
use UnitTestGenerator\Generator\Classes\UnitTestGenerator;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\ReflectionFactory;

class UtgFactory {

    private LoggerInterface $logger;
    private Filesystem $filesystem;
    private ReflectionFactory $reflectionFactory;

    public function __construct(LoggerInterface $logger, Filesystem $filesystem, ReflectionFactory $reflectionFactory) {
        $this->logger = $logger;
        $this->filesystem = $filesystem;
        $this->reflectionFactory = $reflectionFactory;
    }

    public function createRoave(string $inputPath, string $outputPath, string $templatePath, ValuesProvider $valuesProvider): UnitTestGenerator {
        $classTemplate = new ClassTemplate($templatePath, $this->logger, $valuesProvider);
        $pathsHelper = new PathsHelper($inputPath, $outputPath);
        return new UnitTestGenerator($classTemplate, $pathsHelper, $this->logger, $this->filesystem, $this->reflectionFactory);
    }
}
