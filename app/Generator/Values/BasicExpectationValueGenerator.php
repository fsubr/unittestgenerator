<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator\Values;

use UnitTestGenerator\Reflector\MethodReflection;

class BasicExpectationValueGenerator implements ValueGenerator {


    /**
     * @return array<non-empty-string, string|callable>
     */
    public function get(): array {
        $selfClosure = static function (MethodReflection $methodReflection) {
            return 'new '.$methodReflection->getDeclaringClassName().'()';
        };

        return [
            'int' => '1',
            'array' => '[]',
            'bool' => 'true',
            'string' => $this->getString(),
            'float' => '3.14',
            'resource' => 'null',
            'void' => 'null',
            'callable' => 'function () {}',
            'DateTimeInterface' => 'new \DateTime()',
            'self' => $selfClosure,
            'static' => $selfClosure,
            '\static' => $selfClosure,
        ];
    }

    private function getString(): callable {
        return static function (MethodReflection $methodReflection) {
            $reflectionType = $methodReflection->getReturnTypes();
            $datatypeName = $reflectionType[0];
            $str = $datatypeName.sha1($datatypeName);
            return "'".$str."'";
        };
    }
}
