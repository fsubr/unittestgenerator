<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator\Values;

use UnitTestGenerator\Reflector\ParameterReflection;

class BasicParameterValueGenerator implements ValueGenerator {


    /**
     * @return array<non-empty-string, string|callable>
     */
    public function get(): array {
        return [
            'int' => '1',
            'array' => '[]',
            'string' => $this->getString(),
            'bool' => 'true',
            'float' => '3.14',
            'resource' => 'null',
            'null' => 'null',
            'callable' => 'function () {}',
            'DateTimeInterface' => 'new \DateTime()',
            '\DateTimeInterface' => 'new \DateTime()',
        ];
    }

    private function getString(): callable {
        return static function (ParameterReflection $parameterReflection) {
            $str = $parameterReflection->getId();
            return "'".$str."'";
        };
    }
}
