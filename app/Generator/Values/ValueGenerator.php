<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator\Values;

interface ValueGenerator {


    /**
     * @return array<non-empty-string, string|callable>
     */
    public function get(): array;
}
