<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator\Values;

use Psr\Log\LoggerInterface;
use UnitTestGenerator\Reflector\MethodReflection;
use UnitTestGenerator\Reflector\ParameterReflection;

class ValuesProvider {

    private LoggerInterface $logger;
    /**
     * @var array<non-empty-string, string|callable>
     */
    private array $parameterTypes = [];
    /**
     * @var array<non-empty-string, string|callable>
     */
    private array $expectationTypes = [];

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function registerExpectationGenerator(ValueGenerator $generator): void {
        $this->expectationTypes = array_merge($this->expectationTypes, $generator->get());
    }

    public function registerParameterGenerator(ValueGenerator $generator): void {
        $this->parameterTypes = array_merge($this->parameterTypes, $generator->get());
    }

    public function getParameterValue(ParameterReflection $parameterReflection): string {
        if ($parameterReflection->hasDefaultValue()) {
            $defaultValue = $parameterReflection->getDefaultValue();
            if ($defaultValue === []) {
                return '[]';
            }
            return strtolower(var_export($defaultValue, true));
        }

        // TODO beru prvni typ, ale mozna je tohle misto, kde by mohl vznikat dataProvider
        $datatypeName = $parameterReflection->getTypes()[0];
        if (isset($this->parameterTypes[$datatypeName])) {
            if (is_callable($this->parameterTypes[$datatypeName])) {
                return $this->parameterTypes[$datatypeName]($parameterReflection);
            }
            return $this->parameterTypes[$datatypeName];
        }
        $this->logger->info(sprintf('Define mock for %s', $datatypeName));
        if (!str_starts_with($datatypeName, '\\')) {
            $datatypeName = '\\'.$datatypeName;
        }
        return sprintf('$this->createMock(%s::class)', $datatypeName);
    }

    public function getExpectationValue(MethodReflection $methodReflection): string {
        if ($methodReflection->hasReturnType()) {
            $reflectionType = $methodReflection->getReturnTypes();
            $datatypeName = $reflectionType[0];
            if (isset($this->expectationTypes[$datatypeName])) {
                if (is_callable($this->expectationTypes[$datatypeName])) {
                    return $this->expectationTypes[$datatypeName]($methodReflection);
                }
                return $this->expectationTypes[$datatypeName];
            }
            $this->logger->info('Define generic expectation for: '.$datatypeName);
            if (!str_starts_with($datatypeName, '\\')) {
                $datatypeName = '\\'.$datatypeName;
            }
            return sprintf('new %s()', $datatypeName);
        }
        return 'null';
    }
}
