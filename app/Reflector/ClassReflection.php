<?php

/**
 * Facade for Roave and PhpStan class reflection
 */

declare(strict_types=1);

namespace UnitTestGenerator\Reflector;

use ReflectionException;
use ReflectionMethod;
use Roave\BetterReflection\Reflection\ReflectionClass;

class ClassReflection {

    private ReflectionClass $class;

    public function __construct(ReflectionClass $class) {
        $this->class = $class;
    }

    public function getName(): string {
        return $this->class->getName();
    }

    public function getShortName(): string {
        return $this->class->getShortName();
    }

    /**
     * @return list<MethodReflection>
     */
    public function getImmediatePublicMethods(): array {
        $immediateMethods = $this->class->getImmediateMethods(ReflectionMethod::IS_PUBLIC);
        $methodReflection = [];
        foreach ($immediateMethods as $method) {
            $methodReflection[] = new MethodReflection($method);
        }

        return $methodReflection;
    }

    /**
     * @param non-empty-string $name
     *
     * @throws ReflectionException
     */
    public function getMethod(string $name): MethodReflection {
        $method = $this->class->getMethod($name);
        if ($method === null) {
            throw new ReflectionException(sprintf('Error reflecting "%s" method.', $name));
        }
        return new MethodReflection($method);
    }

    public function getNamespaceName(): string {
        return (string)$this->class->getNamespaceName();
    }

    /**
     * @return non-empty-string|null
     */
    public function getFileName(): string|null {
        return $this->class->getFileName();
    }
}
