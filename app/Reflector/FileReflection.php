<?php

/**
 * Facade for Roave file reflection
 */

declare(strict_types=1);

namespace UnitTestGenerator\Reflector;

use Roave\BetterReflection\BetterReflection;
use Roave\BetterReflection\Reflector\DefaultReflector;
use Roave\BetterReflection\SourceLocator\Type\SingleFileSourceLocator;

class FileReflection {

    private DefaultReflector $reflector;

    /**
     * @param non-empty-string $file
     */
    public function __construct(string $file) {
        $astLocator = (new BetterReflection())->astLocator();
        $this->reflector = new DefaultReflector(new SingleFileSourceLocator($file, $astLocator));
    }

    public function getClass(string $name): ClassReflection {
        $class = $this->reflector->reflectClass($name);
        return new ClassReflection($class);
    }

    /**
     * @return list<ClassReflection>
     */
    public function getAllClasses(): array {
        $allClasses = $this->reflector->reflectAllClasses();
        $classes = [];
        foreach ($allClasses as $class) {
            if ($class->isAnonymous() === false) {
                $classes[] = new ClassReflection($class);
            }
        }
        return $classes;
    }
}
