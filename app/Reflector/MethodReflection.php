<?php

/**
 * Facade for Roave and PhpStan method reflection
 */

declare(strict_types=1);

namespace UnitTestGenerator\Reflector;

use PHPStan\PhpDocParser\Ast\PhpDoc\ReturnTagValueNode;
use PHPStan\PhpDocParser\Ast\Type\ArrayShapeNode;
use PHPStan\PhpDocParser\Ast\Type\ArrayTypeNode;
use PHPStan\PhpDocParser\Ast\Type\ConditionalTypeForParameterNode;
use PHPStan\PhpDocParser\Ast\Type\GenericTypeNode;
use PHPStan\PhpDocParser\Ast\Type\IdentifierTypeNode;
use PHPStan\PhpDocParser\Ast\Type\ThisTypeNode;
use PHPStan\PhpDocParser\Ast\Type\TypeNode;
use PHPStan\PhpDocParser\Ast\Type\UnionTypeNode;
use ReflectionException;
use Roave\BetterReflection\Reflection\ReflectionIntersectionType;
use Roave\BetterReflection\Reflection\ReflectionMethod;
use Roave\BetterReflection\Reflection\ReflectionNamedType;
use Roave\BetterReflection\Reflection\ReflectionUnionType;
use RuntimeException;
use UnitTestGenerator\Reflector\PhpDoc\Parser;

class MethodReflection {

    private ReflectionMethod $method;

    public function __construct(ReflectionMethod $method) {
        $this->method = $method;
    }

    /**
     * @return non-empty-string
     */
    public function getName(): string {
        return $this->method->getName();
    }

    public function isConstructor(): bool {
        return $this->method->isConstructor();
    }

    public function getModifiers(): int {
        return $this->method->getModifiers();
    }

    public function hasReturnType(): bool {
        if ($this->method->hasReturnType()) {
            return true;
        }

        $returnTag = $this->getReturnTags();

        return count($returnTag) > 0;
    }

    /**
     * @return string[]
     *
     * @throws ReflectionException
     */
    public function getReturnTypes(): array {
        $reflectionType = $this->method->getReturnType();

        if ($reflectionType === null) {
            return $this->getPhpdocReturnType();
        }

        $types = [];
        if ($reflectionType instanceof ReflectionUnionType) {
            $types = $reflectionType->getTypes();
        }
        if ($reflectionType instanceof ReflectionNamedType) {
            $types = [$reflectionType];
        }
        if ($reflectionType instanceof ReflectionIntersectionType) {
            throw new ReflectionException(sprintf('Intersection type in method "%s" not supported', $this->getName()));
        }

        $datatypeNames = [];
        foreach ($types as $type) {
            if ($type instanceof ReflectionNamedType) {
                $datatypeNames[] = $type->getName();
            }
        }
        return $datatypeNames;
    }

    /**
     * @return list<ParameterReflection>
     */
    public function getParameters(): array {
        $parameters = [];

        foreach ($this->method->getParameters() as $parameter) {
            $parameters[] = new ParameterReflection($parameter, $this->getDocComment());
        }

        return $parameters;
    }

    /**
     * @param non-empty-string $name
     *
     * @throws ReflectionException
     */
    public function getParameter(string $name): ParameterReflection {
        $parameter = $this->method->getParameter($name);
        if ($parameter === null) {
            throw new ReflectionException(sprintf('Error reflecting "%s" parameter.', $name));
        }
        return new ParameterReflection($parameter, $this->getDocComment());
    }

    public function getDeclaringClassName(): string {
        return $this->method->getDeclaringClass()->getShortName();
    }

    /** @return non-empty-string */
    private function getDocComment(): string {
        return $this->method->getDocComment() ?? '/** */';
    }

    /**
     * @return string[]
     *
     * @throws RuntimeException
     */
    private function getPhpdocReturnType(): array {
        $returnTag = $this->getReturnTags();
        $datatypeNames = [];
        foreach ($returnTag as $tag) {
            switch (get_class($tag->type)) {
                case UnionTypeNode::class:
                    /** @var UnionTypeNode $unionTypeNode */
                    $unionTypeNode = $tag->type;
                    foreach ($unionTypeNode->types as $type) {
                        $datatypeNames[] = $this->getType($type);
                    }
                    break;
                case ConditionalTypeForParameterNode::class:
                    // TODO I am pretty sure this is wrong, but I am not sure yet how to implement it the other way
                    /** @var ConditionalTypeForParameterNode $condType */
                    $condType = $tag->type;
                    /** @var UnionTypeNode $ifTypes */
                    $ifTypes = $condType->if;
                    /** @var UnionTypeNode $elseTypes */
                    $elseTypes = $condType->else;
                    foreach (array_merge($ifTypes->types, $elseTypes->types) as $type) {
                        $datatypeNames[] = $this->getType($type);
                    }
                    break;
                default:
                    $datatypeNames[] = $this->getType($tag->type);
            }
        }
        return array_unique($datatypeNames);
    }

    private function getType(TypeNode $node): string {
        switch ($node::class) {
            case ArrayTypeNode::class:
            case ArrayShapeNode::class:
                return 'array';
            case ThisTypeNode::class:
                return $this->method->getDeclaringClass()->getName();
            case IdentifierTypeNode::class:
                return $node->name;
            case GenericTypeNode::class:
                return $this->getType($node->type);
            default:
                $name = $this->method->getDeclaringClass()->getName().'::'.$this->method->getName();
                throw new RuntimeException('Return type '.$node.' of '.$name.' not supported');
        }
    }

    /**
     * @return ReturnTagValueNode[]
     */
    private function getReturnTags(): array {
        $parser = new Parser();
        return $parser->parse($this->getDocComment())->getReturnTagValues();
    }
}
