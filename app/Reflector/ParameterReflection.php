<?php

/**
 * Facade for Roave and PhpStan function parameter reflection
 */

declare(strict_types=1);

namespace UnitTestGenerator\Reflector;

use PHPStan\PhpDocParser\Ast\PhpDoc\ParamTagValueNode;
use PHPStan\PhpDocParser\Ast\Type\ArrayTypeNode;
use PHPStan\PhpDocParser\Ast\Type\CallableTypeNode;
use PHPStan\PhpDocParser\Ast\Type\IdentifierTypeNode;
use PHPStan\PhpDocParser\Ast\Type\TypeNode;
use PHPStan\PhpDocParser\Ast\Type\UnionTypeNode;
use ReflectionException;
use Roave\BetterReflection\NodeCompiler\Exception\UnableToCompileNode;
use Roave\BetterReflection\Reflection\ReflectionIntersectionType;
use Roave\BetterReflection\Reflection\ReflectionNamedType;
use Roave\BetterReflection\Reflection\ReflectionParameter;
use Roave\BetterReflection\Reflection\ReflectionUnionType;
use RuntimeException;
use UnitTestGenerator\Reflector\PhpDoc\Parser;

class ParameterReflection {

    private ReflectionParameter $parameter;
    private string $docComment;

    public function __construct(ReflectionParameter $parameter, string $docComment) {
        $this->parameter = $parameter;
        $this->docComment = $docComment;
    }

    public function getName(): string {
        return $this->parameter->getName();
    }

    // TODO this might not be needed
    public function hasType(): bool {
        if ($this->parameter->hasType()) {
            return true;
        }

        return $this->hasPhpdocType($this->parameter->getName());
    }

    /**
     * @return string[]
     *
     * @throws ReflectionException
     */
    public function getTypes(): array {
        $reflectionType = $this->parameter->getType();

        if ($reflectionType === null) {
            return $this->getPhpdocType($this->parameter->getName());
        }

        $types = [];
        if ($reflectionType instanceof ReflectionUnionType) {
            $types = $reflectionType->getTypes();
        }
        if ($reflectionType instanceof ReflectionNamedType) {
            $types = [$reflectionType];
        }
        if ($reflectionType instanceof ReflectionIntersectionType) {
            throw new ReflectionException(sprintf('Intersection type in parameter "%s" not supported', $this->getName()));
        }

        $datatypeNames = [];
        foreach ($types as $type) {
            if ($type instanceof ReflectionNamedType) {
                $datatypeNames[] = $type->getName();
            }
        }
        return $datatypeNames;
    }

    public function getId(): string {
        if ($this->parameter->getDeclaringClass() === null) {
            return sha1($this->parameter->getDeclaringFunction()->getName().$this->parameter->getName());
        }
        return sha1($this->parameter->getDeclaringClass()->getName().$this->parameter->getDeclaringFunction()->getName().$this->parameter->getName());
    }

    public function isOptional(): bool {
        return $this->parameter->isOptional();
    }

    /**
     * @return scalar|array<scalar>|null
     *
     * @throws UnableToCompileNode
     */
    public function getDefaultValue(): string|int|float|bool|array|null {
        try {
            return $this->parameter->getDefaultValue();
        } catch (UnableToCompileNode $exception) {
            // When default value is not defined global constant (UNDEFINED_CUSTOM_CONSTANT)
            $constantName = $exception->constantName();
            if ($constantName === null) {
                throw $exception;
            }
            return $constantName;
        }
    }

    public function getFileName(): ?string {
        if ($this->parameter->getDeclaringClass() === null) {
            return $this->parameter->getDeclaringFunction()->getFileName();
        }
        return $this->parameter->getDeclaringClass()->getFileName();
    }

    public function hasDefaultValue(): bool {
        return $this->parameter->isDefaultValueAvailable();
    }

    private function hasPhpdocType(string $name): bool {
        $paramTags = $this->getParamTags();
        foreach ($paramTags as $paramTag) {
            if ('$'.$name === $paramTag->parameterName) {
                return $paramTag->type !== null;
            }
        }

        return false;
    }

    /**
     * @return string[]
     *
     * @throws RuntimeException
     */
    private function getPhpdocType(string $name): array {
        $paramTags = $this->getParamTags();
        $datatypeNames = [];
        foreach ($paramTags as $paramTag) {
            if ('$'.$name === $paramTag->parameterName) {
                switch (get_class($paramTag->type)) {
                    case UnionTypeNode::class:
                        /** @var UnionTypeNode $unionTypeNode */
                        $unionTypeNode = $paramTag->type;
                        foreach ($unionTypeNode->types as $type) {
                            $datatypeNames[] = $this->getType($type);
                        }
                        break;
                    default:
                        $datatypeNames[] = $this->getType($paramTag->type);
                        break;
                }
            }
        }

        if (count($datatypeNames) > 0) {
            return array_unique($datatypeNames);
        }

        return ['null'];
    }

    private function getType(TypeNode $node): string {
        switch ($node::class) {
            case ArrayTypeNode::class:
                // TODO add array data types see @param string[]
                return 'array';
            case IdentifierTypeNode::class:
                return $node->name;
            case CallableTypeNode::class:
                // TODO i have no slightest idea what to do with this
                return $node->identifier->name;
            default:
                // TODO use logger
                if ($this->parameter->getDeclaringClass() === null) {
                    $name = $this->parameter->getDeclaringFunction()->getName();
                    throw new RuntimeException('Parameter type '.get_class($node).' of function '.$name.' not supported');
                }
                // When default value is not loaded class
                $name = $this->parameter->getDeclaringClass()->getName().'::'.$this->parameter->getDeclaringFunction()->getName();
                throw new RuntimeException('Parameter type '.get_class($node).' of '.$name.' not supported');
        }
    }

    /**
     * @return ParamTagValueNode[]
     */
    private function getParamTags(): array {
        $parser = new Parser();
        return $parser->parse($this->docComment)->getParamTagValues();
    }
}
