<?php

declare(strict_types=1);

namespace UnitTestGenerator\Reflector\PhpDoc;

use PHPStan\PhpDocParser\Ast\PhpDoc\PhpDocNode;
use PHPStan\PhpDocParser\Lexer\Lexer;
use PHPStan\PhpDocParser\Parser\ConstExprParser;
use PHPStan\PhpDocParser\Parser\PhpDocParser;
use PHPStan\PhpDocParser\Parser\TokenIterator;
use PHPStan\PhpDocParser\Parser\TypeParser;

class Parser {


    public function parse(string $docComment): PhpDocNode {
        $constExprParser = new ConstExprParser();
        $typeParser = new TypeParser($constExprParser);
        $phpDocParser = new PhpDocParser($typeParser, $constExprParser);
        $lexer = new Lexer();
        $tokens = new TokenIterator($lexer->tokenize($docComment));
        return $phpDocParser->parse($tokens);
    }
}
