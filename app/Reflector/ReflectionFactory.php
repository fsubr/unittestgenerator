<?php

declare(strict_types=1);

namespace UnitTestGenerator\Reflector;

use UnitTestGenerator\Reflector\Stream\StreamReflector;

class ReflectionFactory {


    /**
     * @param non-empty-string $file
     */
    public function fromFile(string $file): FileReflection {
        return new FileReflection($file);
    }

    public function streamFromFile(string $file): StreamReflector {
        return new StreamReflector($file);
    }
}
