<?php

/**
 * Stream reflection
 */

declare(strict_types=1);

namespace UnitTestGenerator\Reflector\Stream;

class StreamReflector {

    /**
     * Internal token counter.
     */
    private int $counter = 0;
    /**
     * Source file name.
     */
    private string $filename;
    /**
     * Token array.
     *
     * @var array<int, array<int, int|string>|string>
     */
    private array $tokens = [];

    /**
     * Construct reflector.
     *
     * @param string $filename Source file path
     */
    public function __construct(string $filename) {
        $this->filename = $filename;
    }

    /**
     * Increment token pointer.
     */
    public function next(): bool {
        if ($this->counter < $this->getTokenCount()) {
            $this->counter++;
            return true;
        }
        return false;
    }

    /**
     * Return true if currently streaming Class.
     */
    public function isClass(): bool {
        if ($this->getTokenPartByIndex($this->counter, 0) === T_CLASS && $this->getTokenPartByIndex($this->counter - 1, 1) !== '::') {
            if ($this->getTokenPartByIndex($this->counter - 2, 0) === T_ABSTRACT) {
                return false;
            }
            return true;
        }
        return false;
    }

    public function hasClass(): bool {
        do {
            if ($this->isClass()) {
                return true;
            }
        } while ($this->next());
        return false;
    }

    private function getTokenPartByIndex(int $index, int $part): string|int|false {
        if (!isset($this->tokens[$index]) || $this->getTokenCount() === 0) {
            return false;
        }
        return $this->tokens[$index][$part];
    }

    private function getTokenCount(): int {
        if (count($this->tokens) === 0) {
            $this->tokens = token_get_all((string)file_get_contents($this->filename));
            $this->counter = 0;
        }
        return count($this->tokens);
    }
}
