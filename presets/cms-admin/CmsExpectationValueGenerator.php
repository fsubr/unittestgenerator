<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator;

use UnitTestGenerator\Generator\Values\ValueGenerator;

class CmsExpectationValueGenerator implements ValueGenerator {

    public function get(): array {
        return [
            'int' => '5',
            'float' => '13.14',
            'Shoptet\Accounting\DocumentHistory\TranslateDocumentHistoryProcessor' => 'new \Shoptet\Accounting\DocumentHistory\ProofPaymentTranslateHistoryProcessor()',
            'Shoptet\Import\DocumentItemSaver' => 'new \Shoptet\Import\Helper\ProformaInvoiceItemSaver()',
            'Shoptet\Accounting\Model\ProofPaymentHistory' => "new \Shoptet\Accounting\Model\ProofPaymentHistory(['id' => 1])"
        ];
    }
}
