<?php

declare(strict_types=1);

namespace UnitTestGenerator\Generator;

use UnitTestGenerator\Generator\Values\ValueGenerator;

class CmsParameterValueGenerator implements ValueGenerator {


    /**
     * @return array<non-empty-string, string|callable>
     */
    public function get(): array {
        return [
            /** @var \ProjectDbDriver $projectDbDriver */
            'ProjectDbDriver' => '\ProjectDbDriver::getInstance()',
            'Shoptet\Core\EventManager' => '\Shoptet\Core\EventManager::getInstance()',
        ];
    }

}
