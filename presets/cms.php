<?php

declare(strict_types=1);

/**
 * Shoptet CMS preset
 */

use UnitTestGenerator\Generator\CmsExpectationValueGenerator;
use UnitTestGenerator\Generator\CmsParameterValueGenerator;

$inputPath = 'C:/Users/fsubr\workspace/forge/vagrant/src/cms4/cms/packages';
//$inputPath = 'C:/Users/fsubr\workspace/forge/vagrant/src/cms4';
//$inputPath = '\\\\wsl.localhost\\Debian\\shoptet-src\\cms4\\cms\\packages';
$outputPath = 'c:/Users/fsubr/workspace.old/UTGOutput-cms/';
$templatePath = __DIR__.'/cms-admin';
$exclude = ['tests/', 'vendor/', 'temp/aspect-mock', 'cms/libs', 'cypress_tests', 'migrations/'];

require_once __DIR__.'/cms-admin/CmsParameterValueGenerator.php';
$this->valuesProvider->registerParameterGenerator(new CmsParameterValueGenerator());

require_once __DIR__.'/cms-admin/CmsExpectationValueGenerator.php';
$this->valuesProvider->registerExpectationGenerator(new CmsExpectationValueGenerator());
