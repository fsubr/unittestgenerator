<?php

declare(strict_types=1);

$inputPath = __DIR__.'/../app/';
$outputPath = __DIR__.'/../temp/tests/';
$templatePath = __DIR__.'/utg';
$exclude = ['vendor/'];
