
    public function test{ucmethod}(): void {
        {parametersInit}
        $result = {classname}::{method}({parameters});
        $expected = {expectation};
        self::assertSame($expected, $result);
    }
