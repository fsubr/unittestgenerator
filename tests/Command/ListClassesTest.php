<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Command;

use Exception;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnitTestGenerator\Command\ListClasses;
use UnitTestGenerator\Finder\Classes\FindClasses;
use UnitTestGenerator\Generator\Preset\Preset;
use UnitTestGenerator\Generator\Preset\PresetFactory;
use UnitTestGenerator\Generator\Values\ValuesProvider;

#[CoversClass(ListClasses::class)]
#[UsesClass(Preset::class)]
class ListClassesTest extends TestCase {


    /**
     * @throws Exception
     * @throws \PHPUnit\Framework\MockObject\Exception
     * @throws ExceptionInterface
     */
    public function testRunNoFiles(): void {
        $presetFactory = $this->createMock(PresetFactory::class);
        $valueFactory = $this->createMock(ValuesProvider::class);

        $preset = new Preset('tests/resources/', sys_get_temp_dir(), sys_get_temp_dir(), [], ['php'], $valueFactory);
        $presetFactory->method('load')->willReturn($preset);

        $finder = $this->createMock(FindClasses::class);

        $command = new ListClasses($presetFactory, $finder);
        $input = $this->createInput();
        $output = $this->createOutput();
        $status = $command->run($input, $output);
        self::assertEquals(0, $status);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    private function createInput(): InputInterface {
        $input = $this->createMock(InputInterface::class);
        $input->method('getArgument')->willReturn('default');
        return $input;
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    private function getOutputFormatter(): OutputFormatterInterface {
        $formatter = $this->createMock(OutputFormatterInterface::class);
        $formatter->method('isDecorated')->willReturn(true);
        return $formatter;
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    private function createOutput(): OutputInterface {
        $output = $this->createMock(OutputInterface::class);
        $formatter = $this->getOutputFormatter();
        $output->method('getFormatter')->willReturn($formatter);
        return $output;
    }
}
