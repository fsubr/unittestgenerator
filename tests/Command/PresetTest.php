<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Command;

use Exception;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnitTestGenerator\Command\Preset;

#[CoversClass(Preset::class)]
class PresetTest extends TestCase {


    /**
     * @throws Exception
     * @throws \PHPUnit\Framework\MockObject\Exception
     * @throws ExceptionInterface
     */
    public function testRun(): void {
        $command = new Preset();
        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);
        $status = $command->run($input, $output);
        self::assertEquals(0, $status);
    }
}
