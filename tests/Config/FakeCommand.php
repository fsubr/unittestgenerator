<?php

// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Config;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FakeCommand extends Command {


    protected function configure(): void {
        $this->setName('fake');
        $this->setDescription('Used for testing');
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        return Command::SUCCESS;
    }
}
