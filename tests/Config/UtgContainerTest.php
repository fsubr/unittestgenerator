<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Config;

use Exception;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use UnitTestGenerator\Config\UtgContainer;

#[CoversClass(UtgContainer::class)]
class UtgContainerTest extends TestCase {


    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function testContainerWillStart(): void {
        $container = new UtgContainer([]);
        $app = $container->get(Application::class);
        self::assertInstanceOf(Application::class, $app);
        self::assertTrue($app->has('make'));
        self::assertTrue($app->has('preset'));
        self::assertTrue($app->has('classes'));
        self::assertFalse($app->has('non-sensei'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    public function testApplicationWillRun(): void {
        $container = new UtgContainer([]);
        $app = $container->get(Application::class);
        /** @var Application $app */
        $app->add(new FakeCommand());
        $app->setAutoExit(false);
        $args = new ArgvInput(['./utg.php', 'fake']);
        /** @var OutputInterface $consoleOutput */
        $consoleOutput = $container->get(ConsoleOutput::class);
        $exitCode = $app->run($args, $consoleOutput);
        self::assertSame(0, $exitCode);
    }
}
