<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Filesystem;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use UnitTestGenerator\Filesystem\GenericFilesystem;
use UnitTestGenerator\Tests\MemoryLogger;

#[CoversClass(GenericFilesystem::class)]
class GenericFilesystemTest extends TestCase {

    private string $filename = 'temp/storage/storageFile.txt';
    private string $missingPathFilename = 'temp/actuallyDir.txt';

    protected function tearDown(): void {
        if (file_exists($this->filename)) {
            unlink($this->filename);
            rmdir(dirname($this->filename));
        }
        if (file_exists($this->missingPathFilename)) {
            rmdir($this->missingPathFilename);
        }
    }

    public function testSuccessfulWrite(): void {
        $logger = new MemoryLogger();
        $storage = new GenericFilesystem($logger, 0644);
        $storage->write($this->filename, 'some fish');

        self::assertSame([], $logger->messages);
        self::assertSame('some fish', $storage->read($this->filename));
    }

    public function testFailedWrite(): void {
        mkdir($this->missingPathFilename, 0644, true);
        $logger = new MemoryLogger();
        $storage = new GenericFilesystem($logger, 0644);
        $storage->write($this->missingPathFilename, 'other fish');

        $expected = [
            [
                'error',
                'Error writing to temp/actuallyDir.txt.',
                [],
            ],
        ];
        self::assertSame($expected, $logger->messages);
    }

    public function testFailedCreateDir(): void {
        if (stripos(PHP_OS_FAMILY, 'WIN') !== 0) {
            self::markTestSkipped('Skipped until we found better way to test');
        }
        $logger = new MemoryLogger();
        $storage = new GenericFilesystem($logger, 0644);
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Directory "/a:/wrong" was not created');
        $storage->write('/a:/wrong/path', 'otter fish');
    }

    public function testFailedRead(): void {
        $logger = new MemoryLogger();
        $storage = new GenericFilesystem($logger, 0644);

        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('File some-nonsense.sns not found.');
        $storage->read('some-nonsense.sns');
        $expected = [
            [
                'error',
                'File some-nonsense.sns not found.',
                [],
            ],
        ];
        self::assertSame($expected, $logger->messages);
    }
}
