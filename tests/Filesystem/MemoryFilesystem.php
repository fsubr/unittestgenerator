<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Filesystem;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use UnitTestGenerator\Filesystem\Filesystem;

class MemoryFilesystem implements Filesystem {

    /**
     * @var array<string, string>
     */
    private array $storage = [];

    public function write(string $filename, string $data): void {
        $this->storage[$filename] = $data;
    }

    /**
     * @throws FileNotFoundException
     */
    public function read(string $filename): string {
        if (isset($this->storage[$filename])) {
            return $this->storage[$filename];
        }
        throw new FileNotFoundException(sprintf('File %s not found.', $filename));
    }
}
