<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Filesystem;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

#[CoversClass(MemoryFilesystem::class)]
class MemoryFilesystemTest extends TestCase {


    public function testReadMissing(): void {
        $memory = new MemoryFilesystem();
        $filename = '/root/some/path/file.dat';
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage(sprintf('%s', $filename));
        $memory->read($filename);
    }
}
