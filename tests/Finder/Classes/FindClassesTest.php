<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Finder\Classes;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use UnitTestGenerator\Finder\Classes\FindClasses;
use UnitTestGenerator\Reflector\ReflectionFactory;
use UnitTestGenerator\Reflector\Stream\StreamReflector;

#[CoversClass(FindClasses::class)]
class FindClassesTest extends TestCase {


    /**
     * @throws Exception
     */
    public function testFindFilesWithExtension(): void {
        $reflectionFactory = $this->createMock(ReflectionFactory::class);
        $streamer = $this->createMock(StreamReflector::class);
        $streamer->method('hasClass')->willReturn(true);
        $reflectionFactory->method('streamFromFile')->willReturn($streamer);

        $finder = new FindClasses($reflectionFactory);
        $files = $finder->get('tests/resources', ['php']);

        self::assertCount(12, $files);
    }

    /**
     * @throws Exception
     */
    public function testFindFiles(): void {
        $reflectionFactory = $this->createMock(ReflectionFactory::class);
        $streamer = $this->createMock(StreamReflector::class);
        $streamer->method('hasClass')->willReturn(false);
        $finder = new FindClasses($reflectionFactory);
        $files = $finder->get('tests/resources', ['php', 'txt']);

        self::assertCount(0, $files);
    }

    /**
     * @throws Exception
     */
    public function testExcludePaths(): void {
        $reflectionFactory = $this->createMock(ReflectionFactory::class);
        $streamer = $this->createMock(StreamReflector::class);
        $streamer->method('hasClass')->willReturn(true);
        $reflectionFactory->method('streamFromFile')->willReturn($streamer);
        $finder = new FindClasses($reflectionFactory);
        $files = $finder->get('tests/resources', ['php', 'txt'], ['reference', 'UnitTestGeneratorResource']);

        self::assertCount(19, $files);
        self::assertContains('tests/resources/AllMethodsTestClass.php', $files);
    }
}
