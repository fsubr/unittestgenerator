<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use ReflectionMethod;
use Roave\BetterReflection\Identifier\Identifier;
use Roave\BetterReflection\Identifier\IdentifierType;
use Roave\BetterReflection\Reflector\Exception\IdentifierNotFound;
use RuntimeException;
use UnitTestGenerator\Generator\ClassTemplate;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\ClassReflection;
use UnitTestGenerator\Reflector\MethodReflection;
use UnitTestGenerator\Reflector\ParameterReflection;

#[CoversClass(ClassTemplate::class)]
class ClassTemplateTest extends TestCase {


    /**
     * @throws Exception
     */
    public function testCodeWithoutNamespace(): void {
        $class = $this->createMock(ClassReflection::class);
        $class->method('getNamespaceName')->willReturn('');

        $dynamicMethod = $this->createMock(MethodReflection::class);
        $dynamicMethod->method('isConstructor')->willReturn(false);
        $class->method('getImmediatePublicMethods')->willReturn([$dynamicMethod]);

        $logger = $this->createMock(LoggerInterface::class);
        $valuesProvider = $this->createMock(ValuesProvider::class);
        $template = new ClassTemplate('tests/resources/template', $logger, $valuesProvider);
        $skeleton = $template->render($class);

        self::assertStringNotContainsString('namespace', $skeleton);
    }

    /**
     * @throws Exception
     */
    public function testCodeWithoutNamespaceTemplateWithPrefix(): void {
        $class = $this->createMock(ClassReflection::class);
        $class->method('getNamespaceName')->willReturn('');

        $dynamicMethod = $this->createMock(MethodReflection::class);
        $dynamicMethod->method('isConstructor')->willReturn(false);
        $class->method('getImmediatePublicMethods')->willReturn([$dynamicMethod]);

        $logger = $this->createMock(LoggerInterface::class);
        $valuesProvider = $this->createMock(ValuesProvider::class);
        $template = new ClassTemplate('tests/resources/template-ns-prefix', $logger, $valuesProvider);
        $skeleton = $template->render($class);

        self::assertStringContainsString('namespace tests\unit\app;', $skeleton);
    }

    /**
     * @throws Exception
     */
    public function testCodeWithNamespace(): void {
        $class = $this->createMock(ClassReflection::class);
        $class->method('getNamespaceName')->willReturn('All\Methods\Test\Classes');

        $dynamicMethod = $this->createMock(MethodReflection::class);
        $dynamicMethod->method('isConstructor')->willReturn(false);
        $class->method('getImmediatePublicMethods')->willReturn([$dynamicMethod]);

        $logger = $this->createMock(LoggerInterface::class);
        $valuesProvider = $this->createMock(ValuesProvider::class);
        $template = new ClassTemplate('tests/resources/template', $logger, $valuesProvider);
        $skeleton = $template->render($class);

        self::assertStringContainsString('namespace All\Methods\Test\Classes;', $skeleton);
    }

    /**
     * @throws Exception
     */
    public function testCodeWithNamespaceTemplateWithPrefix(): void {
        $class = $this->createMock(ClassReflection::class);
        $class->method('getNamespaceName')->willReturn('All\Methods\Test\Classes');

        $dynamicMethod = $this->createMock(MethodReflection::class);
        $dynamicMethod->method('isConstructor')->willReturn(false);
        $class->method('getImmediatePublicMethods')->willReturn([$dynamicMethod]);

        $logger = $this->createMock(LoggerInterface::class);
        $valuesProvider = $this->createMock(ValuesProvider::class);
        $template = new ClassTemplate('tests/resources/template-ns-prefix', $logger, $valuesProvider);
        $skeleton = $template->render($class);

        self::assertStringContainsString('namespace tests\unit\app\All\Methods\Test\Classes;', $skeleton);
    }

    /**
     * @throws Exception
     */
    public function testClassHasNoPublicMethods(): void {
        $class = $this->createMock(ClassReflection::class);
        $class->method('getImmediatePublicMethods')->willReturn([]);

        $logger = $this->createMock(LoggerInterface::class);
        $valuesProvider = $this->createMock(ValuesProvider::class);
        $template = new ClassTemplate('', $logger, $valuesProvider);
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('No public methods in  class.');
        $template->render($class);
    }

    /**
     * @throws Exception
     */
    public function testClassHasOnlyConstructor(): void {
        $constructorMethod = $this->createMock(MethodReflection::class);
        $constructorMethod->method('isConstructor')->willReturn(true);
        $constructorMethod->method('getName')->willReturn('constructorMethodName');

        $class = $this->createMock(ClassReflection::class);
        $class->method('getImmediatePublicMethods')->willReturn([$constructorMethod]);

        $logger = $this->createMock(LoggerInterface::class);
        $valuesProvider = $this->createMock(ValuesProvider::class);
        $template = new ClassTemplate('', $logger, $valuesProvider);
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('No test generated for  class with just constructor');
        $template->render($class);
    }

    /**
     * @throws Exception
     */
    public function testFullTemplateRender(): void {
        $class = $this->createMock(ClassReflection::class);
        $class->method('getShortName')->willReturn('ClassShortName');
        $class->method('getNamespaceName')->willReturn('');

        $constructorMethod = $this->createMock(MethodReflection::class);
        $constructorMethod->method('isConstructor')->willReturn(true);
        $constructorMethod->method('getName')->willReturn('constructorMethodName');

        $dynamicMethod = $this->createMock(MethodReflection::class);
        $dynamicMethod->method('isConstructor')->willReturn(false);
        $dynamicMethod->method('getName')->willReturn('methodName');
        $parameter = $this->createMock(ParameterReflection::class);
        $parameter->method('getName')->willReturn('parameterName');
        $secondParameter = $this->createMock(ParameterReflection::class);
        $secondParameter->method('getName')->willReturn('secondParameterName');
        $dynamicMethod->method('getParameters')->willReturn([$parameter, $secondParameter]);

        $staticMethod = $this->createMock(MethodReflection::class);
        $staticMethod->method('isConstructor')->willReturn(false);
        $staticMethod->method('getName')->willReturn('staticMethodName');
        $staticMethod->method('getModifiers')->willReturn(ReflectionMethod::IS_STATIC);

        $class->method('getImmediatePublicMethods')->willReturn([$constructorMethod, $dynamicMethod, $staticMethod]);

        $logger = $this->createMock(LoggerInterface::class);
        $valuesProvider = $this->createMock(ValuesProvider::class);
        $valuesProvider->method('getParameterValue')->willReturn("'value'");
        $valuesProvider->method('getExpectationValue')->willReturn("'expected'");
        $template = new ClassTemplate('tests/resources/template-ns-prefix', $logger, $valuesProvider);
        $skeleton = $template->render($class);

        self::assertStringEqualsFile('tests/resources/reference/FTP-ns-prefix.php.txt', $skeleton);
    }

    /**
     * @throws Exception
     */
    public function testFallbackComment(): void {
        $dynamicMethod = $this->createMock(MethodReflection::class);
        $dynamicMethod->method('isConstructor')->willReturn(false);
        $parameter = $this->createMock(ParameterReflection::class);
        $parameter->method('getName')->willReturn('parameterName');
        $dynamicMethod->method('getParameters')->willReturn([$parameter]);

        $class = $this->createMock(ClassReflection::class);
        $class->method('getImmediatePublicMethods')->willReturn([$dynamicMethod]);

        $logger = $this->createMock(LoggerInterface::class);
        $valuesProvider = $this->createMock(ValuesProvider::class);
        $valuesProvider->method('getParameterValue')->willThrowException(new IdentifierNotFound('parameterName', new Identifier('identifierName', new IdentifierType())));
        $template = new ClassTemplate('tests/resources/template-ns-prefix', $logger, $valuesProvider);
        $skeleton = $template->render($class);

        self::assertStringEqualsFile('tests/resources/reference/FallBackComment.txt', $skeleton);
    }
}
