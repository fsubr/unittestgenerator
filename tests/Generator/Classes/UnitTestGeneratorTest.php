<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator\Classes;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use UnitTestGenerator\Filesystem\Filesystem;
use UnitTestGenerator\Generator\Classes\UnitTestGenerator;
use UnitTestGenerator\Generator\ClassTemplate;
use UnitTestGenerator\Generator\PathsHelper;
use UnitTestGenerator\Generator\UtgFactory;
use UnitTestGenerator\Generator\Values\BasicExpectationValueGenerator;
use UnitTestGenerator\Generator\Values\BasicParameterValueGenerator;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\ClassReflection;
use UnitTestGenerator\Reflector\FileReflection;
use UnitTestGenerator\Reflector\ReflectionFactory;
use UnitTestGenerator\Tests\Filesystem\MemoryFilesystem;
use UnitTestGenerator\Tests\MemoryLogger;

#[CoversClass(UnitTestGenerator::class)]
class UnitTestGeneratorTest extends TestCase {

    /** @var non-empty-string $testFile */
    private string $testFile = 'tests/resources/AllMethodsTestClass2.php';
    private MemoryFilesystem $fileSystem;

    protected function setUp(): void {
        $this->fileSystem = new MemoryFilesystem();
    }

    /**
     * @throws Exception
     */
    public function testGenerator(): void {
        $generator = $this->createUtg('');

        $generator->generate($this->testFile);
        self::assertStringEqualsFile(
            'tests/resources/reference/ReferenceGeneratedSkeleton2.php.txt',
            $this->fileSystem->read('temp/tests/resources/AllMethodsTestClassTest.php')
        );
    }

    /**
     * @throws Exception
     */
    public function testGeneratorWithPaths(): void {
        $generator = $this->createUtg('tests/resources');

        $generator->generate('tests/resources/resource/UnitTestGeneratorResource.php');

        $expected = 'tests/resources/reference/UnitTestGeneratorResourceTest.php.txt';
        $actual = $this->fileSystem->read('temp/resource/UnitTestGeneratorResourceTest.php');

        self::assertStringEqualsFile($expected, $actual);
    }

    /**
     * @throws Exception
     */
    public function testTypeHints(): void {
        $generator = $this->createUtg('tests/resources');

        $generator->generate('tests/resources/TypeParametersClass.php');

        $expected = 'tests/resources/reference/TypeParametersClass.php.txt';
        $actual = $this->fileSystem->read('temp/TypeParametersClassTest.php');

        self::assertStringEqualsFile($expected, $actual);
    }

    /**
     * @throws Exception
     */
    public function testNoEmptyTests(): void {
        $generator = $this->createUtg('tests/resources');

        $generator->generate('tests/resources/NoPublicMethods.php');
        $this->expectException(FileNotFoundException::class);
        $this->fileSystem->read('temp/NoPublicMethodsTest.php');
    }

    /**
     * @throws Exception
     */
    public function testNoEmptyConstructorTests(): void {
        $generator = $this->createUtg('tests/resources');

        $generator->generate('tests/resources/OnlyConstructorMethods.php');
        $this->expectException(FileNotFoundException::class);
        $this->fileSystem->read('temp/OnlyConstructorMethodsTest.php');
    }

    /**
     * @throws Exception
     */
    public function testGenerate(): void {
        $logger = new MemoryLogger();

        $storage = new MemoryFilesystem();
        $classTemplate = $this->createMock(ClassTemplate::class);
        $classTemplate->method('render')->willReturn('some test content');

        $pathsHelper = $this->createMock(PathsHelper::class);
        $pathsHelper->method('getTestPath')->willReturn('some/tests/path/FileTest.php');

        $reflectionFactory = $this->createMock(ReflectionFactory::class);
        $fileReflection = $this->createMock(FileReflection::class);
        $classReflection = $this->createMock(ClassReflection::class);
        $fileReflection->method('getAllClasses')->willReturn([$classReflection]);
        $reflectionFactory->method('fromFile')->willReturn($fileReflection);

        $generator = new UnitTestGenerator($classTemplate, $pathsHelper, $logger, $storage, $reflectionFactory);
        $generator->generate('tests/resources/TypeParametersClass.php');

        self::assertSame([], $logger->messages);
        self::assertSame('some test content', $storage->read('some/tests/path/FileTest.php'));
    }

    /**
     * @throws Exception
     */
    public function testFailToWriteTest(): void {
        if (stripos(PHP_OS_FAMILY, 'WIN') !== 0) {
            self::markTestSkipped('Skipped until we found better way to test');
        }

        $logger = new MemoryLogger();

        $storage = $this->createMock(Filesystem::class);
        $storage->method('write')->willThrowException(new RuntimeException('Directory "/Windows/tests/resources/" was not created'));
        $classTemplate = $this->createMock(ClassTemplate::class);
        $pathsHelper = $this->createMock(PathsHelper::class);
        $reflectionFactory = $this->createMock(ReflectionFactory::class);
        $fileReflection = $this->createMock(FileReflection::class);
        $classReflection = $this->createMock(ClassReflection::class);
        $classReflection->method('getFileName')->willReturn('tests/resources/TypeParametersClass.php');
        $fileReflection->method('getAllClasses')->willReturn([$classReflection]);
        $reflectionFactory->method('fromFile')->willReturn($fileReflection);

        $generator = new UnitTestGenerator($classTemplate, $pathsHelper, $logger, $storage, $reflectionFactory);
        $generator->generate('tests/resources/TypeParametersClass.php');

        $expected = [
            [
                'error',
                'Directory "/Windows/tests/resources/" was not created',
                [],
            ],
            [
                'error',
                'Processing file tests/resources/TypeParametersClass.php',
                [],
            ],
        ];
        self::assertSame($expected, $logger->messages);
    }

    /**
     * @param string $inputPath
     * @return UnitTestGenerator
     * @throws Exception
     */
    private function createUtg(string $inputPath): UnitTestGenerator {
        $templatePath = 'tests/resources/template';
        $logger = $this->createMock(LoggerInterface::class);
        $reflectionFactory = new ReflectionFactory();
        $generatorFactory = new UtgFactory($logger, $this->fileSystem, $reflectionFactory);

        $valuesProvider = new ValuesProvider($logger);
        $valuesProvider->registerExpectationGenerator(new BasicExpectationValueGenerator());
        $valuesProvider->registerParameterGenerator(new BasicParameterValueGenerator());

        return $generatorFactory->createRoave($inputPath, 'temp', $templatePath, $valuesProvider);
    }
}
