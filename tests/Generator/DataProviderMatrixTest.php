<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator;

use Exception;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use UnitTestGenerator\Generator\DataProviderMatrix;

#[CoversClass(DataProviderMatrix::class)]
class DataProviderMatrixTest extends TestCase {


    public function testGenerateTwo(): void {
        $matrix = new DataProviderMatrix();
        $input = [
            'amount' => [-1, 0, 1],
            'flag' => [true, false],
        ];
        $result = $matrix->generate('prefix', $input, true);
        $expected = [
            'prefix-1' => ['amount' => -1, 'flag' => true, 'expected' => true],
            'prefix-2' => ['amount' => -1, 'flag' => false, 'expected' => true],
            'prefix-3' => ['amount' => 0, 'flag' => true, 'expected' => true],
            'prefix-4' => ['amount' => 0, 'flag' => false, 'expected' => true],
            'prefix-5' => ['amount' => 1, 'flag' => true, 'expected' => true],
            'prefix-6' => ['amount' => 1, 'flag' => false, 'expected' => true],
        ];
        self::assertEquals($expected, $result);
    }

    public function testGenerateThree(): void {
        $matrix = new DataProviderMatrix();
        $input = [
            'number' => [-1, 0, 1],
            'force' => [true, false],
            'list' => [[], [1]],
        ];
        $result = $matrix->generate('key', $input, '');
        $expected = [
            'key-1' => ['number' => -1, 'force' => true, 'list' => [], 'expected' => ''],
            'key-2' => ['number' => -1, 'force' => true, 'list' => [1], 'expected' => ''],
            'key-3' => ['number' => -1, 'force' => false, 'list' => [], 'expected' => ''],
            'key-4' => ['number' => -1, 'force' => false, 'list' => [1], 'expected' => ''],
            'key-5' => ['number' => 0, 'force' => true, 'list' => [], 'expected' => ''],
            'key-6' => ['number' => 0, 'force' => true, 'list' => [1], 'expected' => ''],
            'key-7' => ['number' => 0, 'force' => false, 'list' => [], 'expected' => ''],
            'key-8' => ['number' => 0, 'force' => false, 'list' => [1], 'expected' => ''],
            'key-9' => ['number' => 1, 'force' => true, 'list' => [], 'expected' => ''],
            'key-10' => ['number' => 1, 'force' => true, 'list' => [1], 'expected' => ''],
            'key-11' => ['number' => 1, 'force' => false, 'list' => [], 'expected' => ''],
            'key-12' => ['number' => 1, 'force' => false, 'list' => [1], 'expected' => ''],

        ];
        self::assertEquals($expected, $result);
    }

    public function testGenerateFive(): void {
        $matrix = new DataProviderMatrix();
        $input = [
            'count' => [0, 2, 100],
            'list' => [[], [1], ['empty']],
            'enable' => [true, false],
            'enrich' => [true, false],
            'productIds' => [[2, 5], [8, 4, 1]],
        ];
        $result = $matrix->generate('row', $input, new Exception());
        self::assertCount(72, $result);

        $expected = [
            'count' => 0,
            'list' => [],
            'enable' => true,
            'enrich' => true,
            'productIds' => [2, 5],
            'expected' => new Exception(),
        ];
        self::assertEquals($expected, $result['row-1']);

        $expected = [
            'count' => 100,
            'list' => ['empty'],
            'enable' => false,
            'enrich' => false,
            'productIds' => [8, 4, 1],
            'expected' => new Exception(),
        ];
        self::assertEquals($expected, $result['row-72']);
    }
}
