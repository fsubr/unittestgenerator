<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator;

use PhpParser\BuilderFactory;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\CloningVisitor;
use PhpParser\NodeVisitorAbstract;
use PhpParser\ParserFactory;
use PhpParser\PhpVersion;
use PhpParser\Node;
use PhpParser\PrettyPrinter\Standard;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\TestCase;

#[CoversNothing]
class ModifyClassTest extends TestCase {


    public function testModifyClass(): void {
        $code = '<?php
            class MyClass {
            
                public function existingMethod($param) {
                    return $param;    
                }
                
            }';

        $parser = (new ParserFactory())->createForVersion(PhpVersion::getHostVersion());
        $stmts = $parser->parse($code);
        $oldTokens = $parser->getTokens();

        $factory = new BuilderFactory();
        $a = new Node\Expr\Assign(new Node\Expr\Variable('password'), $factory->val('abcd'));

        $assert = new Node\Expr\StaticCall(new Node\Name('self'), 'assertEquals', [
            new Node\Arg(new Variable('expected')),
            new Node\Arg(new Variable('actual')),
            new Node\Arg(new Variable('message')),
        ]);

        $newMethod = $factory->method('testNewMethod')->makePublic()->setDocComment('/** This is a new method **/')->addStmt($a)->addStmt($assert)->getNode();

        $traverser = new NodeTraverser();
        $traverser->addVisitor(new CloningVisitor());
        $traverser->addVisitor(new class ($newMethod) extends NodeVisitorAbstract {
            private Node $newMethod;

            public function __construct(ClassMethod $newMethod) {
                $this->newMethod = $newMethod;
            }

            public function leaveNode(Node $node) {
                if ($node instanceof Node\Stmt\Class_) {
                    $node->stmts[] = $this->newMethod;
                }
                return null;
            }
        });
        $newStmts = $traverser->traverse($stmts);

        $prettyPrinter = new Standard();
        $result = $prettyPrinter->printFormatPreserving($newStmts, $stmts, $oldTokens);
        self::assertStringContainsString('existingMethod', $result);
        self::assertStringContainsString('testNewMethod', $result);
    }
}
