<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use UnitTestGenerator\Generator\PathsHelper;

#[CoversClass(PathsHelper::class)]
class PathsHelperTest extends TestCase {


    public function testGetTestPath(): void {
        $classFile = 'c:/users/bin/SomeClass.php';
        $className = 'SomeClass';

        $paths = new PathsHelper('c:/users', 'c:/users/temp/phpUnit');

        self::assertEquals('c:/users/temp/phpUnit/bin/SomeClassTest.php', $paths->getTestPath($classFile, $className));
    }

    public function testGetTestOutsidePath(): void {
        $classFile = '/generator\StatsTest.php';
        $className = 'SomeClass';

        $paths = new PathsHelper(
            'c:/Documents and Settings/filda/workspace/Glogster/bin/Stats/',
            'c:/Documents and Settings/filda/workspace/UnitTestGenerator/GlogsterTest/'
        );

        $expected = 'c:/Documents and Settings/filda/workspace/UnitTestGenerator/GlogsterTest/generator/SomeClassTest.php';
        self::assertEquals($expected, $paths->getTestPath($classFile, $className));
    }

    public function testGetTestOutsidePathSlashes(): void {
        $classFile = 'c:/Documents and Settings/fsubr/workspace/Glogster/bin/Stats\Exports\comandos_exports.php';
        $className = 'ComandosExport';

        $paths = new PathsHelper(
            'c:/Documents and Settings/fsubr/workspace/Glogster/bin/Stats/',
            'c:/Documents and Settings/fsubr/workspace/UnitTestGenerator/GlogsterTest/'
        );

        $expected = 'c:/Documents and Settings/fsubr/workspace/UnitTestGenerator/GlogsterTest/Exports/ComandosExportTest.php';

        self::assertEquals($expected, $paths->getTestPath($classFile, $className));
    }
}
