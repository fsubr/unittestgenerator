<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator\Preset;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use UnitTestGenerator\Generator\Preset\Preset;
use UnitTestGenerator\Generator\Values\ValuesProvider;

#[CoversClass(Preset::class)]
class ConfigTest extends TestCase {


    /**
     * @throws Exception
     */
    public function testConfig(): void {
        $inputPath = 'input';
        $outputPath = 'output';
        $templatePath = 'template';
        $exclude = ['ex', 'clude'];
        $extensions = ['php', 'php3'];
        $valueFactory = $this->createMock(ValuesProvider::class);

        $preset = new Preset($inputPath, $outputPath, $templatePath, $exclude, $extensions, $valueFactory);

        self::assertEquals($inputPath, $preset->getInputPath());
        self::assertEquals($outputPath, $preset->getOutputPath());
        self::assertEquals($templatePath, $preset->getTemplatePath());
        self::assertEquals($exclude, $preset->getExclude());
        self::assertEquals($extensions, $preset->getExtensions());
    }
}
