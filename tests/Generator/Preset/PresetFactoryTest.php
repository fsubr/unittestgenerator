<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator\Preset;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use UnitTestGenerator\Generator\Preset\Preset;
use UnitTestGenerator\Generator\Preset\PresetFactory;
use UnitTestGenerator\Generator\Preset\PresetNotFound;
use UnitTestGenerator\Generator\Values\ValuesProvider;

#[CoversClass(PresetFactory::class)]
#[UsesClass(Preset::class)]
class PresetFactoryTest extends TestCase {


    /**
     * @throws PresetNotFound
     * @throws Exception
     */
    public function testLoad(): void {
        $valueFactory = $this->createMock(ValuesProvider::class);
        $factory = new PresetFactory($valueFactory);
        $preset = $factory->load('cms');
        $expected = ['tests/', 'vendor/', 'temp/aspect-mock', 'cms/libs', 'cypress_tests', 'migrations/'];
        self::assertEquals($expected, $preset->getExclude());
        self::assertEquals(['php'], $preset->getExtensions());
    }

    /**
     * @throws Exception
     */
    public function testMissingPresetThrowsException(): void {
        $valueFactory = $this->createMock(ValuesProvider::class);
        $factory = new PresetFactory($valueFactory);
        $this->expectException(PresetNotFound::class);
        $factory->load('prdel');
    }
}
