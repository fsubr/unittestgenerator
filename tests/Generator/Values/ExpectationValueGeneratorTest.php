<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator\Values;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use UnitTestGenerator\Generator\Values\BasicExpectationValueGenerator;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\FileReflection;
use UnitTestGenerator\Tests\resources\MethodsReturnTypes;
use UnitTestGenerator\Tests\MemoryLogger;

#[CoversClass(BasicExpectationValueGenerator::class)]
class ExpectationValueGeneratorTest extends TestCase {


    /**
     * @param non-empty-string $method
     *
     * @throws ReflectionException
     */
    #[DataProvider('expectationDataProvide')]
    public function testGenerate(string $method, string $expectation): void {
        $reflector = new FileReflection('tests/resources/MethodsReturnTypes.php');
        $class = $reflector->getClass(MethodsReturnTypes::class);

        $reflection = $class->getMethod($method);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);
        $factory->registerExpectationGenerator(new BasicExpectationValueGenerator());

        $result = $factory->getExpectationValue($reflection);
        self::assertSame($expectation, $result);
    }

    /**
     * @return array<string, array{method: string, expectation: string}>
     */
    public static function expectationDataProvide(): array {
        return [
            'String' => ['method' => 'withReflection', 'expectation' => "'stringecb252044b5ea0f679ee78ec1a12904739e2904d'"],
            'Nullable string' => ['method' => 'withReflectionUnion', 'expectation' => "'stringecb252044b5ea0f679ee78ec1a12904739e2904d'"],
            'Array' => ['method' => 'withDocblock', 'expectation' => '[]'],
            'Array/Int union' => ['method' => 'withDocblockUnion', 'expectation' => '[]'],
            'Inconsistent phpdoc' => ['method' => 'mixedDeclaration', 'expectation' => '1'],
            'Missing type' => ['method' => 'missingType', 'expectation' => 'null'],
            'With phpdoc class array' => ['method' => 'getPackagesByStatus', 'expectation' => '[]'],
            'With phpdoc class' => ['method' => 'getDbRow', 'expectation' => 'new \Dibi\Row()'],
            'With phpdoc typed array' => ['method' => 'getUsableServices', 'expectation' => '[]'],
            'With phpdoc conditional typed array' => ['method' => 'getPrice', 'expectation' => '1'],
            'With phpdoc mixed array' => ['method' => 'getModules', 'expectation' => '[]'],
            'With phpdoc array shape' => ['method' => 'getVideoCodeFromUrl', 'expectation' => '[]'],
            'With phpdoc array shape 2' => ['method' => 'roundPriceParts', 'expectation' => '[]'],
            'With phpdoc multi array shape' => ['method' => 'getErrors', 'expectation' => '[]'],
            'Return this' => ['method' => 'setHasModifyUrls', 'expectation' => 'new \UnitTestGenerator\Tests\resources\MethodsReturnTypes()'],
            'Callable' => ['method' => 'simpleCallable', 'expectation' => 'function () {}'],
            'Datetime' => ['method' => 'createDate', 'expectation' => 'new \DateTime()'],
        ];
    }
}
