<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator\Values;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use UnitTestGenerator\Generator\Values\ValueGenerator;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\MethodReflection;
use UnitTestGenerator\Tests\MemoryLogger;

#[CoversClass(ValuesProvider::class)]
class ExpectationValueProviderTest extends TestCase {


    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testReturnTypeNotDefined(): void {
        $reflection = $this->createMock(MethodReflection::class);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);

        $result = $factory->getExpectationValue($reflection);
        self::assertSame('null', $result);
        self::assertSame([], $logger->messages);
    }

    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testReturnArbitraryType(): void {
        $reflection = $this->createMock(MethodReflection::class);
        $reflection->method('hasReturnType')->willReturn(true);
        $reflection->method('getReturnTypes')->willReturn(['string']);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);

        $result = $factory->getExpectationValue($reflection);
        self::assertSame('new \string()', $result);
        $expected = [
            'info',
            'Define generic expectation for: string',
            [],
        ];
        self::assertSame($expected, $logger->messages[0]);
    }

    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testExpectationGenerator(): void {
        $firstMethod = $this->createMock(MethodReflection::class);
        $firstMethod->method('hasReturnType')->willReturn(true);
        $firstMethod->method('getReturnTypes')->willReturn(['string']);

        $secondMethod = $this->createMock(MethodReflection::class);
        $secondMethod->method('hasReturnType')->willReturn(true);
        $secondMethod->method('getReturnTypes')->willReturn(['int']);

        $firstGenerator = $this->createMock(ValueGenerator::class);
        $firstGenerator->method('get')->willReturn(['string' => 'list-of-characters']);

        $secondGenerator = $this->createMock(ValueGenerator::class);
        $secondGenerator->method('get')->willReturn(['int' => '81']);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);
        $factory->registerExpectationGenerator($firstGenerator);
        $factory->registerExpectationGenerator($secondGenerator);

        $result = $factory->getExpectationValue($firstMethod);
        self::assertSame('list-of-characters', $result);

        $result = $factory->getExpectationValue($secondMethod);
        self::assertSame('81', $result);

        self::assertSame([], $logger->messages);
    }

    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testCallableExpectationGenerator(): void {
        $reflection = $this->createMock(MethodReflection::class);
        $reflection->method('hasReturnType')->willReturn(true);
        $reflection->method('getReturnTypes')->willReturn(['string']);
        $reflection->method('getDeclaringClassName')->willReturn('AllMethodsClass');

        $generator = $this->createMock(ValueGenerator::class);

        $selfClosure = static function (MethodReflection $methodReflection) {
            return 'new '.$methodReflection->getDeclaringClassName().'()';
        };
        $generator->method('get')->willReturn(['string' => $selfClosure]);
        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);
        $factory->registerExpectationGenerator($generator);

        $result = $factory->getExpectationValue($reflection);
        self::assertSame('new AllMethodsClass()', $result);
        self::assertSame([], $logger->messages);
    }
}
