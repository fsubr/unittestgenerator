<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator\Values;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use UnitTestGenerator\Generator\Values\BasicParameterValueGenerator;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\FileReflection;
use UnitTestGenerator\Tests\resources\parameters\Typehints;
use UnitTestGenerator\Tests\MemoryLogger;

#[CoversClass(BasicParameterValueGenerator::class)]
class ParameterValueGeneratorTest extends TestCase {


    /**
     * @param non-empty-string $method
     * @param non-empty-string $parameter
     *
     * @throws ReflectionException
     */
    #[DataProvider('expectationDataProvider')]
    public function testGetValue(string $method, string $parameter, string $expectation): void {
        $reflector = new FileReflection('tests/resources/parameters/Typehints.php');
        $class = $reflector->getClass(Typehints::class);

        $reflection = $class->getMethod($method);

        $parameterReflection = $reflection->getParameter($parameter);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);
        $factory->registerParameterGenerator(new BasicParameterValueGenerator());

        $result = $factory->getParameterValue($parameterReflection);
        self::assertSame($expectation, $result);
    }

    /**
     * @return array<string, array{method: string, parameter: string, expectation: string}>
     */
    public static function expectationDataProvider(): array {
        return [
            'Reflection union type' => ['method' => 'onlyTypeHints', 'parameter' => 'number', 'expectation' => '1'],
            'Reflection without type' => ['method' => 'onlyTypeHints', 'parameter' => 'password', 'expectation' => 'null'],
            'Reflection nullable type' => ['method' => 'onlyTypeHints', 'parameter' => 'force', 'expectation' => 'false'],
            'Reflection array type' => ['method' => 'onlyTypeHints', 'parameter' => 'flags', 'expectation' => '[]'],
            'Reflection class type' => ['method' => 'notBuiltIn', 'parameter' => 'entity', 'expectation' => '$this->createMock(\MyApp\UserEntity::class)'],
            'Phpdoc union type' => ['method' => 'onlyPhpDoc', 'parameter' => 'ratio', 'expectation' => '1'],
            'Phpdoc string union type' => ['method' => 'clearTokenRedisCacheDirectly', 'parameter' => 'accessTokens', 'expectation' => "'e2b4318a99674c9afc21654c97aa43e68ea9c08a'"],
            'Phpdoc without type' => ['method' => 'onlyPhpDoc', 'parameter' => 'name', 'expectation' => 'null'],
            'Phpdoc nullable type' => ['method' => 'onlyPhpDoc', 'parameter' => 'switch', 'expectation' => 'false'],
            'Phpdoc array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'sizes', 'expectation' => '[]'],
            'Phpdoc typed array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'attributes', 'expectation' => '[]'],
            'Phpdoc class type' => ['method' => 'onlyPhpDoc', 'parameter' => 'entity', 'expectation' => '$this->createMock(\MyApp\UserEntity::class)'],
            'Combined definition mismatch' => ['method' => 'combinedDefinition', 'parameter' => 'url', 'expectation' => "'ceed8dc54ac19773ffa417f8d7c76f9ef25866e7'"],
            'Combined with type' => ['method' => 'combinedDefinition', 'parameter' => 'https', 'expectation' => 'true'],
            'Completely without type' => ['method' => 'combinedDefinition', 'parameter' => 'local', 'expectation' => 'null'],
            'Intended "array" without type' => ['method' => 'combinedDefinition', 'parameter' => 'array', 'expectation' => 'null'],
            'Callable' => ['method' => 'callableParam', 'parameter' => 'fun', 'expectation' => 'function () {}'],
            'Datetime' => ['method' => 'formatDate', 'parameter' => 'date', 'expectation' => 'new \DateTime()'],
            'Phpdoc datetime' => ['method' => 'formatDocDate', 'parameter' => 'date', 'expectation' => 'new \DateTime()'],

            // FIXXME this is wrong
            'Callable template' => ['method' => 'findAdminNewsByFilter', 'parameter' => 'filterOrMutator', 'expectation' => '$this->createMock(\T::class)'],
            'Variadic argument' => ['method' => 'setForcedMax', 'parameter' => 'values', 'expectation' => 'null'],
        ];
    }
}
