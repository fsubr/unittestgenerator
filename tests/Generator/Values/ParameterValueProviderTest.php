<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Generator\Values;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use UnitTestGenerator\Generator\Values\ValueGenerator;
use UnitTestGenerator\Generator\Values\ValuesProvider;
use UnitTestGenerator\Reflector\MethodReflection;
use UnitTestGenerator\Reflector\ParameterReflection;
use UnitTestGenerator\Tests\MemoryLogger;

#[CoversClass(ValuesProvider::class)]
class ParameterValueProviderTest extends TestCase {


    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testTypeNotDefined(): void {
        $reflection = $this->createMock(ParameterReflection::class);
        $reflection->method('getTypes')->willReturn(['null']);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);

        $result = $factory->getParameterValue($reflection);
        self::assertSame('$this->createMock(\null::class)', $result);
        $expected = [
            'info',
            'Define mock for null',
            [],
        ];
        self::assertSame($expected, $logger->messages[0]);
    }

    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testDefaultType(): void {
        $reflection = $this->createMock(ParameterReflection::class);
        $reflection->method('hasDefaultValue')->willReturn(true);
        $reflection->method('getDefaultValue')->willReturn(false);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);

        $result = $factory->getParameterValue($reflection);
        self::assertSame('false', $result);
        self::assertSame([], $logger->messages);
    }

    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testDefaultArrayType(): void {
        $reflection = $this->createMock(ParameterReflection::class);
        $reflection->method('hasDefaultValue')->willReturn(true);
        $reflection->method('getDefaultValue')->willReturn([]);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);

        $result = $factory->getParameterValue($reflection);
        self::assertSame('[]', $result);
        self::assertSame([], $logger->messages);
    }

    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testParameterGenerator(): void {
        $firstParameter = $this->createMock(ParameterReflection::class);
        $firstParameter->method('hasDefaultValue')->willReturn(false);
        $firstParameter->method('getTypes')->willReturn(['string']);

        $secondParameter = $this->createMock(ParameterReflection::class);
        $secondParameter->method('hasDefaultValue')->willReturn(false);
        $secondParameter->method('getTypes')->willReturn(['int']);

        $firstGenerator = $this->createMock(ValueGenerator::class);
        $firstGenerator->method('get')->willReturn(['string' => 'list-of-characters']);

        $secondGenerator = $this->createMock(ValueGenerator::class);
        $secondGenerator->method('get')->willReturn(['int' => '81']);

        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);
        $factory->registerParameterGenerator($firstGenerator);
        $factory->registerParameterGenerator($secondGenerator);

        $result = $factory->getParameterValue($firstParameter);
        self::assertSame('list-of-characters', $result);

        $result = $factory->getParameterValue($secondParameter);
        self::assertSame('81', $result);

        self::assertSame([], $logger->messages);
    }

    /**
     * @throws Exception
     * @throws ReflectionException
     */
    public function testCallableParameterGenerator(): void {
        $reflection = $this->createMock(ParameterReflection::class);
        $reflection->method('hasDefaultValue')->willReturn(false);
        $reflection->method('getTypes')->willReturn(['string']);
        $reflection->method('getId')->willReturn('Parameter-Id');

        $generator = $this->createMock(ValueGenerator::class);

        $closure = static function (ParameterReflection $parameterReflection) {
            $str = $parameterReflection->getId();
            return "'".$str."'";
        };
        $generator->method('get')->willReturn(['string' => $closure]);
        $logger = new MemoryLogger();
        $factory = new ValuesProvider($logger);
        $factory->registerParameterGenerator($generator);

        $result = $factory->getParameterValue($reflection);
        self::assertSame("'Parameter-Id'", $result);
        self::assertSame([], $logger->messages);
    }
}
