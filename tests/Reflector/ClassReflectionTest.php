<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Reflector;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use UnitTestGenerator\Reflector\ClassReflection;
use UnitTestGenerator\Reflector\FileReflection;
use UnitTestGenerator\Tests\resources\MethodsReturnTypes;

#[CoversClass(ClassReflection::class)]
#[UsesClass(FileReflection::class)]
class ClassReflectionTest extends TestCase {

    private ClassReflection $class;
    /** @var non-empty-string */
    private string $file = 'tests/resources/MethodsReturnTypes.php';

    protected function setUp(): void {
        $reflector = new FileReflection($this->file);
        $this->class = $reflector->getClass(MethodsReturnTypes::class);
    }

    public function testGetFilename(): void {
        self::assertSame($this->file, $this->class->getFileName());
    }

    public function testGetMethod(): void {
        $this->expectException(ReflectionException::class);
        $this->expectExceptionMessage('Error reflecting "nopeNope" method.');
        $this->class->getMethod('nopeNope');
    }
}
