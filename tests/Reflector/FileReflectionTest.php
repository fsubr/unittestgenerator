<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Reflector;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;
use UnitTestGenerator\Reflector\ClassReflection;
use UnitTestGenerator\Reflector\FileReflection;

#[CoversClass(FileReflection::class)]
#[UsesClass(ClassReflection::class)]
class FileReflectionTest extends TestCase {

    /** @var non-empty-string */
    private string $file = 'tests/resources/resource/AnonymousClass.php';
    private FileReflection $reflector;

    protected function setUp(): void {
        $this->reflector = new FileReflection($this->file);
    }

    public function testSkipAnonymousClasses(): void {
        $classes = $this->reflector->getAllClasses();
        self::assertCount(1, $classes);
        self::assertSame('AnonymousClass', $classes[0]->getShortName());
    }
}
