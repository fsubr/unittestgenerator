<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Reflector;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use UnitTestGenerator\Reflector\ClassReflection;
use UnitTestGenerator\Reflector\FileReflection;
use UnitTestGenerator\Reflector\MethodReflection;
use UnitTestGenerator\Reflector\ParameterReflection;
use UnitTestGenerator\Reflector\PhpDoc\Parser;
use UnitTestGenerator\Tests\resources\MethodsReturnTypes;
use UnitTestGenerator\Tests\resources\parameters\Typehints;

#[CoversClass(MethodReflection::class)]
#[UsesClass(ClassReflection::class)]
#[UsesClass(FileReflection::class)]
#[UsesClass(ParameterReflection::class)]
#[UsesClass(Parser::class)]
class MethodReflectionTest extends TestCase {

    private ClassReflection $class;

    protected function setUp(): void {
        $reflector = new FileReflection('tests/resources/MethodsReturnTypes.php');
        $this->class = $reflector->getClass(MethodsReturnTypes::class);
    }

    /**
     * @param non-empty-string $method
     *
     * @throws ReflectionException
     */
    #[DataProvider('hasReturnTypeProvider')]
    public function testHasReturnType(string $method, bool $expectation): void {
        $reflection = $this->class->getMethod($method);
        self::assertSame($expectation, $reflection->hasReturnType());
    }

    /**
     * @return array<string, array{method: string, expectation: bool}>
     */
    public static function hasReturnTypeProvider(): array {
        return [
            'With reflection' => ['method' => 'withReflection', 'expectation' => true],
            'With phpdoc' => ['method' => 'withDocblock', 'expectation' => true],
            'Mixed' => ['method' => 'mixedDeclaration', 'expectation' => true],
            'Missing' => ['method' => 'missingType', 'expectation' => false],
            'With phpdoc class array' => ['method' => 'getPackagesByStatus', 'expectation' => true],
        ];
    }

    /**
     * @param non-empty-string $method
     * @param string[] $expectation
     *
     * @throws ReflectionException
     */
    #[DataProvider('getReturnTypesProvider')]
    public function testGetReturnTypes(string $method, array $expectation): void {
        $reflection = $this->class->getMethod($method);
        self::assertSame($expectation, $reflection->getReturnTypes());
    }

    /**
     * @return array<string, array{method: string, expectation: string[]}>
     */
    public static function getReturnTypesProvider(): array {
        return [
            'With reflection' => ['method' => 'withReflection', 'expectation' => ['string']],
            'With reflection union' => ['method' => 'withReflectionUnion', 'expectation' => ['string', 'null']],
            'With reflection class' => ['method' => 'fooType', 'expectation' => ['Some\Foo']],
            'With phpdoc' => ['method' => 'withDocblock', 'expectation' => ['array']],
            'With phpdoc union' => ['method' => 'withDocblockUnion', 'expectation' => ['array', 'int']],
            'Mixed' => ['method' => 'mixedDeclaration', 'expectation' => ['int']],
            'Missing' => ['method' => 'missingType', 'expectation' => []],
            'With phpdoc class array' => ['method' => 'getPackagesByStatus', 'expectation' => ['array']],
            'With phpdoc typed array' => ['method' => 'getUsableServices', 'expectation' => ['array']],
            'With phpdoc conditional typed array' => ['method' => 'getPrice', 'expectation' => ['int', 'null', 'float']],
            'With phpdoc mixed array' => ['method' => 'getModules', 'expectation' => ['array']],
            'With phpdoc array shape' => ['method' => 'getVideoCodeFromUrl', 'expectation' => ['array', 'null']],
            'With phpdoc array shape 2' => ['method' => 'roundPriceParts', 'expectation' => ['array']],
            'With phpdoc multi array shape' => ['method' => 'getErrors', 'expectation' => ['array', 'null']],
            'Return this' => ['method' => 'setHasModifyUrls', 'expectation' => ['UnitTestGenerator\Tests\resources\MethodsReturnTypes']],
        ];
    }

    /**
     * @param non-empty-string $method
     * @param string[] $expectation
     *
     * @throws ReflectionException
     */
    #[DataProvider('getParametersProvider')]
    public function testGetParameters(string $method, array $expectation): void {
        $reflector = new FileReflection('tests/resources/parameters/Typehints.php');
        $class = $reflector->getClass(Typehints::class);

        $reflection = $class->getMethod($method);

        $parameters = $reflection->getParameters();
        $names = [];
        foreach ($parameters as $parameter) {
            $names[] = $parameter->getName();
        }

        self::assertSame($expectation, $names);
    }

    /**
     * @return array<string, array{method: string, expectation: string[]}>
     */
    public static function getParametersProvider(): array {
        return [
            'With reflection' => ['method' => 'onlyTypeHints', 'expectation' => ['number', 'password', 'flags', 'force']],
        ];
    }

    /**
     * @throws ReflectionException
     */
    public function testGetMethodDeclaringClassName(): void {
        $method = $this->class->getMethod('withReflectionUnion');
        self::assertSame('MethodsReturnTypes', $method->getDeclaringClassName());
    }

    public function testMissingParameter(): void {
        $this->expectException(ReflectionException::class);
        $this->expectExceptionMessage('Error reflecting "no-param" parameter.');
        $method = $this->class->getMethod('getPrice');
        $method->getParameter('no-param');
    }

    /**
     * @throws ReflectionException
     */
    public function testUnsupportedIntersection(): void {
        $method = $this->class->getMethod('intersection');
        $this->expectException(ReflectionException::class);
        $this->expectExceptionMessage('Intersection type in method "intersection" not supported');
        $method->getReturnTypes();
    }
}
