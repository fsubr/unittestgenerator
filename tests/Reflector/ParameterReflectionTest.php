<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Reflector;

use LogicException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use Roave\BetterReflection\Reflector\Exception\IdentifierNotFound;
use Throwable;
use UnitTestGenerator\Reflector\ClassReflection;
use UnitTestGenerator\Reflector\FileReflection;
use UnitTestGenerator\Reflector\MethodReflection;
use UnitTestGenerator\Reflector\ParameterReflection;
use UnitTestGenerator\Reflector\PhpDoc\Parser;
use UnitTestGenerator\Tests\resources\parameters\Typehints;

#[CoversClass(ParameterReflection::class)]
#[UsesClass(ClassReflection::class)]
#[UsesClass(FileReflection::class)]
#[UsesClass(MethodReflection::class)]
#[UsesClass(Parser::class)]
class ParameterReflectionTest extends TestCase {

    private ClassReflection $class;

    protected function setUp(): void {
        $reflector = new FileReflection('tests/resources/parameters/Typehints.php');
        $this->class = $reflector->getClass(Typehints::class);
    }

    /**
     * @param non-empty-string $method
     * @param non-empty-string $parameter
     *
     * @throws ReflectionException
     */
    #[DataProvider('hasTypeProvider')]
    public function testHasType(string $method, string $parameter, bool $expectation): void {
        $methodReflection = $this->class->getMethod($method);
        $reflection = $methodReflection->getParameter($parameter);
        self::assertSame($expectation, $reflection->hasType());
    }

    /**
     * @return array<string, array{method: string, parameter: string, expectation: bool}>
     */
    public static function hasTypeProvider(): array {
        return [
            'Reflection with type' => ['method' => 'onlyTypeHints', 'parameter' => 'force', 'expectation' => true],
            'Reflection without type' => ['method' => 'onlyTypeHints', 'parameter' => 'password', 'expectation' => false],
            'Reflection array type' => ['method' => 'onlyTypeHints', 'parameter' => 'flags', 'expectation' => true],
            'Reflection class type' => ['method' => 'notBuiltIn', 'parameter' => 'entity', 'expectation' => true],
            'Phpdoc with type' => ['method' => 'onlyPhpDoc', 'parameter' => 'switch', 'expectation' => true],
            'Phpdoc without type' => ['method' => 'onlyPhpDoc', 'parameter' => 'name', 'expectation' => false],
            'Phpdoc array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'sizes', 'expectation' => true],
            'Phpdoc typed array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'attributes', 'expectation' => true],
            'Combined definition mismatch' => ['method' => 'combinedDefinition', 'parameter' => 'url', 'expectation' => true],
            'Completely without type' => ['method' => 'combinedDefinition', 'parameter' => 'local', 'expectation' => false],
            'Intended "array" type' => ['method' => 'combinedDefinition', 'parameter' => 'array', 'expectation' => false],
        ];
    }

    /**
     * @param non-empty-string $method
     * @param non-empty-string $parameter
     * @param string[] $expectation
     *
     * @throws ReflectionException
     */
    #[DataProvider('getTypeProvider')]
    public function testGetType(string $method, string $parameter, array $expectation): void {
        $methodReflection = $this->class->getMethod($method);
        $reflection = $methodReflection->getParameter($parameter);
        self::assertSame($expectation, $reflection->getTypes());
    }

    /**
     * @return array<string, array{method: string, parameter: string, expectation: string[]}>
     */
    public static function getTypeProvider(): array {
        return [
            'Reflection union type' => ['method' => 'onlyTypeHints', 'parameter' => 'number', 'expectation' => ['int', 'float']],
            'Reflection without type' => ['method' => 'onlyTypeHints', 'parameter' => 'password', 'expectation' => ['null']],
            'Reflection nullable type' => ['method' => 'onlyTypeHints', 'parameter' => 'force', 'expectation' => ['bool', 'null']],
            'Reflection array type' => ['method' => 'onlyTypeHints', 'parameter' => 'flags', 'expectation' => ['array']],
            'Reflection class type' => ['method' => 'notBuiltIn', 'parameter' => 'entity', 'expectation' => ['MyApp\UserEntity']],
            'Phpdoc union type' => ['method' => 'onlyPhpDoc', 'parameter' => 'ratio', 'expectation' => ['int', 'float']],
            'Phpdoc string union type' => ['method' => 'clearTokenRedisCacheDirectly', 'parameter' => 'accessTokens', 'expectation' => ['string', 'array']],
            'Phpdoc without type' => ['method' => 'onlyPhpDoc', 'parameter' => 'name', 'expectation' => ['null']],
            'Phpdoc nullable type' => ['method' => 'onlyPhpDoc', 'parameter' => 'switch', 'expectation' => ['bool', 'null']],
            'Phpdoc array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'sizes', 'expectation' => ['array']],
            'Phpdoc typed array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'attributes', 'expectation' => ['array']],
            'Combined definition mismatch' => ['method' => 'combinedDefinition', 'parameter' => 'url', 'expectation' => ['string']],
            'Combined with type' => ['method' => 'combinedDefinition', 'parameter' => 'https', 'expectation' => ['bool']],
            'Completely without type' => ['method' => 'combinedDefinition', 'parameter' => 'local', 'expectation' => ['null']],
            'Intended "array" without type' => ['method' => 'combinedDefinition', 'parameter' => 'array', 'expectation' => ['null']],
            'Callable template' => ['method' => 'findAdminNewsByFilter', 'parameter' => 'filterOrMutator', 'expectation' => ['T', 'callable']],
            'Variadic argument' => ['method' => 'setForcedMax', 'parameter' => 'values', 'expectation' => ['null']],
        ];
    }

    /**
     * @param non-empty-string $method
     * @param non-empty-string $parameter
     *
     * @throws ReflectionException
     */
    #[DataProvider('getNameProvider')]
    public function testGetName(string $method, string $parameter): void {
        $methodReflection = $this->class->getMethod($method);
        $reflection = $methodReflection->getParameter($parameter);
        self::assertSame($parameter, $reflection->getName());
    }

    /**
     * @return array<string, array{method: string, parameter: string}>
     */
    public static function getNameProvider(): array {
        return [
            'Reflection union type' => ['method' => 'onlyTypeHints', 'parameter' => 'number'],
            'Reflection without type' => ['method' => 'onlyTypeHints', 'parameter' => 'password'],
            'Reflection nullable type' => ['method' => 'onlyTypeHints', 'parameter' => 'force'],
            'Phpdoc without type' => ['method' => 'onlyPhpDoc', 'parameter' => 'name'],
            'Phpdoc nullable type' => ['method' => 'onlyPhpDoc', 'parameter' => 'switch'],
            'Phpdoc array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'sizes'],
            'Combined definition mismatch' => ['method' => 'combinedDefinition', 'parameter' => 'url'],
            'Intended "array" without type' => ['method' => 'combinedDefinition', 'parameter' => 'array'],
        ];
    }

    /**
     * @param non-empty-string $method
     * @param non-empty-string $parameter
     *
     * @throws ReflectionException
     */
    #[DataProvider('getIdProvider')]
    public function testGetId(string $method, string $parameter, string $expectation): void {
        $methodReflection = $this->class->getMethod($method);
        $reflection = $methodReflection->getParameter($parameter);
        self::assertSame($expectation, $reflection->getId());
    }

    /**
     * @return array<string, array{method: string, parameter: string, expectation: string}>
     */
    public static function getIdProvider(): array {
        return [
            'Reflection with type' => ['method' => 'onlyTypeHints', 'parameter' => 'force', 'expectation' => 'b494dcf5e42e3c83c48980581fe84b678f992ced'],
            'Reflection without type' => ['method' => 'onlyTypeHints', 'parameter' => 'password', 'expectation' => 'c260323452b5042a3a7b0ab7f6de738055327921'],
            'Reflection array type' => ['method' => 'onlyTypeHints', 'parameter' => 'flags', 'expectation' => '0147791bbd8543548756d7777c72c8c5105702c5'],
            'Reflection class type' => ['method' => 'notBuiltIn', 'parameter' => 'entity', 'expectation' => '84127e8fa78ac344664e2d26e05885e91ee695f7'],
            'Phpdoc with type' => ['method' => 'onlyPhpDoc', 'parameter' => 'switch', 'expectation' => '388785c2c236180690fcca6265192fb3500c567d'],
            'Phpdoc without type' => ['method' => 'onlyPhpDoc', 'parameter' => 'name', 'expectation' => 'dff88cc7872106d158c91ddc17aa4ea89d394282'],
            'Phpdoc array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'sizes', 'expectation' => '95fe0ad916d590f4e2ddd42d7fc05998a3db4824'],
            'Combined definition mismatch' => ['method' => 'combinedDefinition', 'parameter' => 'url', 'expectation' => 'ceed8dc54ac19773ffa417f8d7c76f9ef25866e7'],
            'Completely without type' => ['method' => 'combinedDefinition', 'parameter' => 'local', 'expectation' => '128070a1f490b4d212a2a4f407c181d62675e6e2'],
            'Intended "array" type' => ['method' => 'combinedDefinition', 'parameter' => 'array', 'expectation' => 'cab0357d1ae91ba10fccf88da6540c6fbc4325d0'],
            'Undefined class' => ['method' => 'undefinedClassConstant', 'parameter' => 'enum', 'expectation' => 'f3921f5b3c2bc51422d7b15be6713f4611853d41'],
        ];
    }

    /**
     * @param non-empty-string $method
     * @param non-empty-string $parameter
     *
     * @throws ReflectionException
     */
    #[DataProvider('isOptionalProvider')]
    public function testIsOptional(string $method, string $parameter, bool $expectation): void {
        $methodReflection = $this->class->getMethod($method);
        $reflection = $methodReflection->getParameter($parameter);
        self::assertSame($expectation, $reflection->isOptional());
    }

    /**
     * @return array<string, array{method: string, parameter: string, expectation: bool}>
     */
    public static function isOptionalProvider(): array {
        return [
            'Reflection with type' => ['method' => 'onlyTypeHints', 'parameter' => 'force', 'expectation' => true],
            'Reflection without type' => ['method' => 'onlyTypeHints', 'parameter' => 'password', 'expectation' => false],
            'Reflection array type' => ['method' => 'onlyTypeHints', 'parameter' => 'flags', 'expectation' => false],
            'Reflection class type' => ['method' => 'notBuiltIn', 'parameter' => 'entity', 'expectation' => false],
            'Phpdoc with type' => ['method' => 'onlyPhpDoc', 'parameter' => 'switch', 'expectation' => true],
            'Phpdoc without type' => ['method' => 'onlyPhpDoc', 'parameter' => 'name', 'expectation' => false],
            'Phpdoc array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'sizes', 'expectation' => false],
            'Combined definition mismatch' => ['method' => 'combinedDefinition', 'parameter' => 'url', 'expectation' => false],
            'Completely without type' => ['method' => 'combinedDefinition', 'parameter' => 'local', 'expectation' => false],
            'Intended "array" type' => ['method' => 'combinedDefinition', 'parameter' => 'array', 'expectation' => false],
            'Undefined constant' => ['method' => 'undefinedConstant', 'parameter' => 'number', 'expectation' => true],
            'Undefined class' => ['method' => 'undefinedClassConstant', 'parameter' => 'enum', 'expectation' => true],
            'Variadic' => ['method' => 'setForcedMax', 'parameter' => 'values', 'expectation' => true],
        ];
    }

    /**
     * @throws ReflectionException
     */
    public function testGetFileName(): void {
        $method = $this->class->getMethod('undefinedClassConstant');
        $reflection = $method->getParameter('enum');
        self::assertSame('tests/resources/parameters/Typehints.php', $reflection->getFileName());
    }

    /**
     * @param non-empty-string $method
     * @param non-empty-string $parameter
     *
     * @throws ReflectionException
     */
    #[DataProvider('getDefaultValueProvider')]
    public function testGetDefaultValue(string $method, string $parameter, mixed $expectation): void {
        $methodReflection = $this->class->getMethod($method);
        $reflection = $methodReflection->getParameter($parameter);
        self::assertSame($expectation, $reflection->getDefaultValue());
    }

    /**
     * @return array<string, array{method: string, parameter: string, expectation: bool|string}>
     */
    public static function getDefaultValueProvider(): array {
        return [
            'Reflection with type' => ['method' => 'onlyTypeHints', 'parameter' => 'force', 'expectation' => false],
            'Phpdoc with type' => ['method' => 'onlyPhpDoc', 'parameter' => 'switch', 'expectation' => false],
            // TODO check out isDefaultValueConstant() maybe it can help
            'Undefined constant' => ['method' => 'undefinedConstant', 'parameter' => 'number', 'expectation' => 'UNDEFINED_CUSTOM_CONSTANT'],
        ];
    }

    /**
     * @param non-empty-string $method
     * @param non-empty-string $parameter
     * @param class-string<Throwable> $expectation
     *
     * @throws ReflectionException
     */
    #[DataProvider('getDefaultValueProviderException')]
    public function testGetDefaultValueException(string $method, string $parameter, string $expectation): void {
        $methodReflection = $this->class->getMethod($method);
        $reflection = $methodReflection->getParameter($parameter);
        $this->expectException($expectation);
        $reflection->getDefaultValue();
    }

    /**
     * @return array<string, array{method: string, parameter: string, expectation: string}>
     */
    public static function getDefaultValueProviderException(): array {
        return [
            'Undefined class' => ['method' => 'undefinedClassConstant', 'parameter' => 'enum', 'expectation' => IdentifierNotFound::class],
            'Reflection class type' => ['method' => 'notBuiltIn', 'parameter' => 'entity', 'expectation' => LogicException::class],
            'Reflection array type' => ['method' => 'onlyTypeHints', 'parameter' => 'flags', 'expectation' => LogicException::class],
            'Intended "array" type' => ['method' => 'combinedDefinition', 'parameter' => 'array', 'expectation' => LogicException::class],
            'Reflection without type' => ['method' => 'onlyTypeHints', 'parameter' => 'password', 'expectation' => LogicException::class],
            'Phpdoc array type' => ['method' => 'onlyPhpDoc', 'parameter' => 'sizes', 'expectation' => LogicException::class],
            'Combined definition mismatch' => ['method' => 'combinedDefinition', 'parameter' => 'url', 'expectation' => LogicException::class],
            'Phpdoc without type' => ['method' => 'onlyPhpDoc', 'parameter' => 'name', 'expectation' => LogicException::class],
            'Completely without type' => ['method' => 'combinedDefinition', 'parameter' => 'local', 'expectation' => LogicException::class],
            'Variadic' => ['method' => 'setForcedMax', 'parameter' => 'values', 'expectation' => LogicException::class],
        ];
    }
}
