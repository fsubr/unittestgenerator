<?php

declare(strict_types=1);

namespace UnitTestGenerator\Tests\Reflector\Stream;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use UnitTestGenerator\Reflector\Stream\StreamReflector;

#[CoversClass(StreamReflector::class)]
class StreamReflectorTest extends TestCase {

    private StreamReflector $reflector;

    protected function setUp(): void {
        $this->reflector = new StreamReflector('tests/resources/AllMethodsTestClass.php');
    }

    /**
     * Just for covering.
     *
     * FIXXME nejde to jinak?
     */
    public function testCoverage(): void {
        self::assertFalse($this->reflector->isClass());
    }
}
