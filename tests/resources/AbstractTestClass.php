<?php

namespace UTG\Some\Names;

abstract class AbstractTestClass {


    public function __construct() {
    }

    abstract public function abstractMethod($param);

}
