<?php

namespace All\Methods\Test\Classes;

class AllMethodsTestClass {
    public const VALUE = 5;


    public function __construct() {

    }

    /**
     * @param int[] $pole TODO Dej to k parametru $a
     */
    public function publicMethod($a, $b = false, $pole = []) {
        shuffle($pole);
        if (!$b) {
            return $a;
        }
        return $b;
    }

    function defaultFunction() {

    }

    static function staticFunction() {

    }

    public function returnSelf(): self
    {
        return new self();
    }

    /**
     * @param $a
     * @param string[] $requiredAttributes
     */
    public static function publicStaticMethod($a, $requiredAttributes) {
        return $a;
    }

    public function returnString(int $param): string
    {
        return 'Number: '.$param;
    }

    public function returnStringNull(): ?string
    {
    }

    public function defaultValueIsClass($otherType = self::VALUE, int $hintedType = TypeEnum::NUMBER)
    {

    }

    public function markAsPaid(int $orderId, ?string $paid, Logger $logger = null): bool {
    }

    /**
     * Get percent
     *
     * @param int|float $percent
     * @return int|FALSE
     */
    public function getPercent($percent)
    {

    }

    protected function protectedMethod() {

    }

    protected static function protectedStaticMethod() {
        return SecondClass::class;
    }

    private function privateMethod($a) {
        return $a;
    }

}

class SecondClass {


    public function secondPublicMethod($a = false) {
    }

    private function secondPrivateMethod() {
    }

}
