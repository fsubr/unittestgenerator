<?php
/**
 * @package Glogster
 * @subpackage Users
 * @author Petr Malina <petr.malina@wdf.cz>
 */
require_once(Glogster_Config::getPath('bin') . "users.php");

class Glogster_Users
{

    public function search($query = array(), $page = 1, $limit = 20, $edu = false, $teacher = false, $trial = false,$commando = false, $ambassador = false)
    {

        // Replace the search characters to use with MySQL
        if(preg_match('/[*?]/', $query)) {
            $query = str_replace(array('*', '?'), array('%', '_'), $query);
        } else {
            $query = /*'%' .*/ $query . '%';
        }

        if(!$limit) { $limit = 20; }
        if(!$page) { $page = 1; }

        // Get the offset from the page
        $offset = ($page - 1) * $limit;

        try {
            $pdo = Glogster_PDO::getInstanceSlave();

            $teacherCondition = '';
            if($edu) {
                //$teacherCondition = $teacher ? ' AND `users`.`edu` = 0' : ' AND `users`.`edu` IS NOT NULL';
                $teacherCondition .= ' AND `users`.`edu_old` = 0';
            }

            if ($commando || $ambassador) {
                $teacherCondition.=' AND `users`.`commando` = 1 ';
                if ($ambassador) {
                    $teacherCondition.=' AND `users`.`edu_old` > -1 ';
                } else {
                    $teacherCondition.=' AND `users`.`edu_old` = -1 ';
                }
            }

            $searchCondition = "`users`.`nick_ascii` LIKE :QUERY";
            if (strpos($query, '@') !== false) {
                $searchCondition = "email like :QUERY";
            }

            $stm = $pdo->prepare("SELECT id, nick, email FROM `users` WHERE " . $searchCondition . " AND status != 'deleted'" . $teacherCondition ." ORDER BY nick LIMIT :OFFSET,:LIMIT;");
            $stm->bindValue(':QUERY', $query, PDO::PARAM_STR);
            $stm->bindValue(':OFFSET', $offset, PDO::PARAM_INT);
            if($edu && $teacher)
            {
                $stm->bindValue(':LIMIT', 1000, PDO::PARAM_INT);
            }
            else
            {
                $stm->bindValue(':LIMIT', $limit, PDO::PARAM_INT);
            }
            $stm->execute();

            if ($data = $stm->fetchAll()) {

                // ugly teacher hack
                if($edu && $teacher) {
                    foreach($data as $k=>$v)
                    {
                        if(!Glogster_User::isTeacher($v['id'])) unset($data[$k]);

                        // the most uglier teacher hack I can realize
                        if($trial && !Glogster_EduHelper::isTrialEdu($v['id'])) unset($data[$k]);
                    }
                }

                return $data;
            }

            return false;
        } catch (PDOException $e) {
            throw new Exception('DB error: ' . $e->getMessage());
        }
    }


    public function changePassword($id, $password, $retype)
    {
        if(!$id) {
            throw new Exception('No user specified.');
        }

        if(!preg_match('/^[0-9A-Za-z]{5,}$/',$password)) {
            throw new Exception('Password can only contain 0-9,A-Z and a-z.');
        }

        if($password != $retype) {
            throw new Exception('Passwords are different.');
        }

        $password = md5($password);

        try {
            $pdo = Glogster_PDO::getInstanceMaster();

            $stm = $pdo->prepare("UPDATE `users` SET `password` = :PASSWORD WHERE `id` = :ID;");
            $stm->bindValue(':ID', $id, PDO::PARAM_INT);
            $stm->bindValue(':PASSWORD', $password, PDO::PARAM_STR);

            $stm->execute();
            Glogster_User::invalidateCache($id);
        } catch (PDOException $e) {
            throw new Exception('DB error: ' . $e->getMessage());
        }
    }


    public function deleteAvatar($id, $gender = 'M')
    {
        if(!$id) {
            throw new Exception('No user specified.');
        }

        if($gender!='M' && $gender!='F') {
            throw new Exception('Wrong gender.');
        }

        $avatar = $gender == 'M' ? 1 : 2;

        try {
            $pdo = Glogster_PDO::getInstanceMaster();

            $stm = $pdo->prepare("UPDATE `users` SET `avatar` = :AVATAR WHERE `id` = :ID;");
            $stm->bindValue(':ID', $id, PDO::PARAM_INT);
            $stm->bindValue(':AVATAR', $avatar, PDO::PARAM_INT);

            $stm->execute();
            Glogster_User::invalidateCache($id);
        } catch (PDOException $e) {
            throw new Exception('DB error: ' . $e->getMessage());
        }
    }


    public function setCommando($id,$on = true)
    {
        if(!$id) {
            throw new Exception('No user specified.');
        }

        try {
            $pdo = Glogster_PDO::getInstanceMaster();
            if ($on)
            $stm = $pdo->prepare("UPDATE `users` SET `commando` = 1 WHERE `id` = :ID;");
            else
            $stm = $pdo->prepare("UPDATE `users` SET `commando` = 0 WHERE `id` = :ID;");
            $stm->bindValue(':ID', $id, PDO::PARAM_INT);

            $stm->execute();
            Glogster_User::invalidateCache($id);
        } catch (PDOException $e) {
            throw new Exception('DB error: ' . $e->getMessage());
        }
    }

    public function deleteMood($id)
    {
        if(!$id) {
            throw new Exception('No user specified.');
        }

        try {
            $pdo = Glogster_PDO::getInstanceMaster();

            $stm = $pdo->prepare("UPDATE `users` SET `mood` = NULL WHERE `id` = :ID;");
            $stm->bindValue(':ID', $id, PDO::PARAM_INT);

            $stm->execute();
            Glogster_User::invalidateCache($id);
        } catch (PDOException $e) {
            throw new Exception('DB error: ' . $e->getMessage());
        }
    }

}