<?php

namespace UnitTestGenerator\Tests\resources;

use Shoptet\Localization\Model\Currency;
use Shoptet\Products\Helper\PriceHelper;

class MethodsReturnTypes {

    public function withReflection(): string
    {

    }

    public function withReflectionUnion(): ?string
    {

    }

    /**
     * @return array
     */
    public function withDocblock()
    {

    }

    /**
     * @return array|int
     */
    public function withDocblockUnion()
    {

    }

    /**
     * @return float
     */
    public function mixedDeclaration(): int
    {

    }

    /**
     * @return float|bool
     */
    public function mixedDeclarationUnion(): int|string
    {

    }

    public function missingType()
    {

    }

    public function returnSelf(): self
    {

    }

    public function noReturn(): void
    {

    }

    /**
     * @param int $status
     * @return \Dibi\Row[]
     */
    public static function getPackagesByStatus($status)
    {

    }

    /**
     * @return \Dibi\Row
     */
    public static function getDbRow($status) {}

    /**
     * @return array<int,string|string>
     */
    public static function getUsableServices() {}

    /**
     * @param string|null $priceType
     * @return ($priceType is 'taxClassId' ? int|null : float|null)
     */
    public function getPrice($priceType = null) {}

    /**
     * @return mixed[][]|\Dibi\Row[]
     */
    public static function getModules() {}

    /**
     * @param string $url
     * @return array{code: string, type: string}|null
     */
    public static function getVideoCodeFromUrl(string $url) {}

    /**
     * @return array<array{line:mixed,text:mixed}>|null
     */
    public static function getErrors() {}

    /**
     * @return $this
     */
    public function setHasModifyUrls($modifyUrl) {}

    /**
     * @return array{withVat: float, withoutVat: float, vat: float}
     */
    public static function roundPriceParts() {}

    public function fooType(): \Some\Foo {}

    public function simpleCallable(): callable {}

    public function createDate(): \DateTimeInterface {}

    public function intersection(): Foo&Bar {}
}
