<?php

namespace UnitTestGenerator\Tests\resources;

class NoPublicMethods {

    private function somePrivateFunction() {}

    private function otherPrivateFunction() {}
}
