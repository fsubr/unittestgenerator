<?php

namespace UnitTestGenerator\Tests\resources;

class OnlyConstructorMethods {

    public function __construct() {}

    private function somePrivateMethod() {}
}
