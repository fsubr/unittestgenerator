<?php

namespace UnitTestGenerator\resources;

class TypeParametersClass {

    /**
     * @param int $a
     * @param bool $c
     * @param array $pole
     * @param SomeClass $class
     * @param string $d
     * @param int $b
     * @return User\Address
     */
    public function foo(int $a, $c, array $pole, SomeClass $class, $d = 'word', int $b = 5): User\Address {

    }

    /**
     * @param string $number
     * @return int
     */
    public function typeMismatch(int $number = 1): int {

    }

    public function allDataTypes(int $integer, array $array, string $string, bool $bool, float $float): bool {

    }

    /**
     * @param int $integer
     * @param array $array
     * @param string $string
     * @param bool $bool
     * @param float $float
     * @return int
     */
    public function definedDocBlock($integer, $array, $string, $bool, $float): int {

    }
}
