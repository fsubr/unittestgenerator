<?php

namespace UnitTestGenerator\Tests\resources\parameters;

use Shoptet\Information\Entity\AdminNewsEntity;
use Shoptet\Information\Service\AdminNewsResourceFilter;

class Typehints
{

    public function onlyTypeHints(int|float $number, $password, array $flags, ?bool $force = false): ?array {

    }

    /**
     * @param int|float $ratio
     * @param $name
     * @param array $sizes
     * @param string[] $attributes
     * @param bool|null $switch
     * @param \MyApp\UserEntity $entity
     * @return array|null
     */
    public function onlyPhpDoc($ratio, $name, $sizes, $attributes, $entity, $switch = false): ?array {

    }

    /**
     * @param int $url
     * @param bool $https
     * @return void
     */
    public function combinedDefinition(string $url, bool $https, $local, $array): void {

    }

    public function notBuiltIn(\MyApp\UserEntity $entity): ?LoggedUser {

    }

    public function undefinedConstant($number = UNDEFINED_CUSTOM_CONSTANT) {

    }

    public function undefinedClassConstant(int $enum = \Util\Permissions::WRITE) {

    }

    /**
     * Can be used outside only for privateApi tokens!
     *
     * @param string|string[] $accessTokens
     */
    public static function clearTokenRedisCacheDirectly($accessTokens): void {

    }

    /**
     * @template T of AdminNewsResourceFilter
     * @param T|callable(T):void $filterOrMutator
     * @return array<AdminNewsEntity>
     */
    public function findAdminNewsByFilter($filterOrMutator): array {}

    public function setForcedMax(...$values): void {}

    public function callableParam(callable $fun) {}

    public function formatDate(\DateTimeInterface $date) {}

    /**
     * @param \DateTimeInterface $date
     * @return void
     */
    public function formatDocDate($date) {}
}

