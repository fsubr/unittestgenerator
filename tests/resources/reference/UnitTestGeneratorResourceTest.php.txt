<?php


use tests\unit\ShoptetUnit;

class UnitTestGeneratorResourceTest extends TestCase {
    /** @var UnitTestGeneratorResource */
    private $unitTestGeneratorResource;

    protected function setUp(): void {
        $number = 1;
        $variable = null;
        $this->unitTestGeneratorResource = new UnitTestGeneratorResource($number, $variable);
    }

    protected function tearDown(): void {
    }

    public function testPublicMethod(): void {
        $this->markTestIncomplete('Automatically generated');
        $a = null;
        $b = false;
        $result = $this->unitTestGeneratorResource->publicMethod($a, $b);
        $expected = null;
        self::assertEquals($expected, $result);
    }

    public function testDefaultFunction(): void {
        $this->markTestIncomplete('Automatically generated');
        $result = $this->unitTestGeneratorResource->defaultFunction();
        $expected = null;
        self::assertEquals($expected, $result);
    }

    public function testStaticFunction(): void {
        $this->markTestIncomplete('Automatically generated');
        $result = UnitTestGeneratorResource::staticFunction();
        $expected = null;
        self::assertEquals($expected, $result);
    }

    public function testPublicStaticMethod(): void {
        $this->markTestIncomplete('Automatically generated');
        $a = null;
        $result = UnitTestGeneratorResource::publicStaticMethod($a);
        $expected = null;
        self::assertEquals($expected, $result);
    }
}
