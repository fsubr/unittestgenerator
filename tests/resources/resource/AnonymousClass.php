<?php

namespace UnitTestGenerator\resources\resource;

class AnonymousClass
{
    public static function registerHandler($prefix = null): void
    {
            session_set_save_handler(new class () implements \SessionHandlerInterface {

                public function write($id, $data): bool
                {
                    return false;
                }

                public function destroy($id): bool
                {
                    return false;
                }

                public function gc($max_lifetime)
                {
                    return 0;
                }

                public function close(): bool
                {
                    return false;
                }

                public function open(string $path, string $name): bool
                {
                    return false;
                }

                public function read(string $id): string|false
                {
                    return false;
                }
            }, true);
    }
}
