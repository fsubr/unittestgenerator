<?php
class UnitTestGeneratorResource extends ParentUnitClass {


    public function __construct(int $number, $variable) {

    }

    public function publicMethod($a, $b = false) {
        if (!$b) {
            return $a;
        }
        return $b;
    }

    function defaultFunction() {

    }

    static function staticFunction() {

    }

    public static function publicStaticMethod($a) {
        return $a;
    }

    protected function protectedMethod() {

    }

    protected static function protectedStaticMethod() {

    }

    private function privateMethod($a) {
        return $a;
    }

}

class OtherUnitTestGeneratorResource {

    public function secondPublicMethod($a = false) {
    }

    private function secondPrivateMethod() {
    }

}
