<?php

namespace {namespace};

use tests\unit\ShoptetUnit;

class {classname}Test extends TestCase {
    /** @var {classname} */
    private ${lcclassname};
    {methods}
}
