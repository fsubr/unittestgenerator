#!/usr/bin/env php
<?php

declare(strict_types=1);

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\ErrorHandler\Debug;
use UnitTestGenerator\Config\UtgContainer;

// https://fsymbols.com/generators/carty/
echo '
█░█ ▀█▀ █▀▀
█▄█ ░█░ █▄█

';

Debug::enable();

$container = new UtgContainer([]);

try {
    $application = $container->get(Application::class);
    $args = new ArgvInput($argv);
    $consoleOutput = $container->get(ConsoleOutput::class);
    $application->run($args, $consoleOutput);
} catch (Throwable $e) {
    echo sprintf("\033[31;1mCommand failed (%s)!\033[0m\n", $e->getMessage());
}
